﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StringRepositoryUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Sahara.Data.UnitTests
{
    using System;
    using System.Data.SqlClient;
    using System.Globalization;
    using NUnit.Framework;

    /// <summary>
    /// Implements unit tests for the <see cref="StringRepository"/> class.
    /// </summary>
    [TestFixture]
    [Category("Integration")]
    public class StringRepositoryUnitTests : RepositoryTestFixture<IStringRepository, StringRepository>
    {
        /// <summary>
        /// Tests if new string table entries are created by the <see cref="StringRepository.GetStringId"/> method.
        /// </summary>
        [Test]
        public void GetNewStringId()
        {
            string str = Guid.NewGuid().ToString();
            int stringId = this.Repository.GetStringId(str);

            object savedValue = this.ExecuteQuery(
                "select Value from dbo.StringTable where StringId = @StringId", 
                new SqlParameter("@StringId", stringId));

            Assert.That(stringId, Is.GreaterThan(0));
            Assert.That(savedValue, Is.EqualTo(str));
        }

        /// <summary>
        /// Tests if the <see cref="StringRepository.GetStringId"/> method properly gets identifiers of strings which
        /// are already added to the string table.
        /// </summary>
        [Test]
        public void GetExistingStringId()
        {
            string value = Guid.NewGuid().ToString();
            object strId = this.ExecuteQuery(
                "insert into dbo.StringTable (Value) values (@str); select scope_identity()",
                new SqlParameter("@str", value));
            int id = Convert.ToInt32(strId, CultureInfo.InvariantCulture);

            int stringId = this.Repository.GetStringId(value);
            
            Assert.That(stringId, Is.EqualTo(id));
        }

        /// <summary>
        /// Tests if the <see cref="StringRepository.GetStringId"/> method throws a <see cref="ArgumentNullException"/>
        /// if the specified argument is <c>null</c>.
        /// </summary>
        [Test]
        public void GetIdForNullString()
        {
            var exception = Assert.Throws<ArgumentNullException>(() => this.Repository.GetStringId(null));
            Assert.That(exception.ParamName, Is.EqualTo("str"));
        }

        /// <summary>
        /// Tests if the <see cref="StringRepository.GetString"/> method properly returns the values for strings which
        /// are present in the string table.
        /// </summary>
        [Test]
        public void GetStringForExistingId()
        {
            string value = Guid.NewGuid().ToString();
            object strId = this.ExecuteQuery(
                "insert into dbo.StringTable (Value) values (@str); select scope_identity()",
                new SqlParameter("@str", value));
            int id = Convert.ToInt32(strId, CultureInfo.InvariantCulture);

            string str = this.Repository.GetString(id);

            Assert.That(str, Is.EqualTo(value));
        }

        /// <summary>
        /// Tests if the <see cref="StringRepository.GetString"/> method returns <c>null</c> if the there is no string
        /// with the specified identifier in the string table.
        /// </summary>
        [Test]
        public void GetStringForNonExistingId()
        {
            string str = this.Repository.GetString(int.MaxValue);

            Assert.That(str, Is.Null);
        }
    }
}
