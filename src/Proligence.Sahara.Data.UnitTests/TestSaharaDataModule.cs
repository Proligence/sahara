﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TestSaharaDataModule.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Sahara.Data.UnitTests
{
    using Autofac;
    using Proligence.Astoria.Configuration;
    using Proligence.Astoria.Mocks;
    using Proligence.Sahara.Configuration;

    /// <summary>
    /// Configures dependency injection for testing.
    /// </summary>
    internal class TestSaharaDataModule : SaharaDataModule
    {
        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <param name="builder">The builder through which components can be registered.</param>
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType(typeof(GrainRepository));
            builder.RegisterType(typeof(StringRepository));

            RegisterMockedConfiguration(builder);
        }

        /// <summary>
        /// Registers mocked service configuration in the container.
        /// </summary>
        /// <param name="builder">The container builder.</param>
        private static void RegisterMockedConfiguration(ContainerBuilder builder)
        {
            var configurationProvider = new MockConfigurationProvider<SaharaConfiguration>(
@"<SaharaConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Sahara.Configuration"">
  <Database>
    <SqlDatabase>Sahara</SqlDatabase>
    <SqlServer>junktown.proligence.pl</SqlServer>
  </Database>
</SaharaConfiguration>");

            SaharaConfiguration mockConfiguration = configurationProvider.GetConfiguration("dummy");
            builder.RegisterInstance(mockConfiguration)
                .As(typeof(SaharaConfiguration), typeof(AstoriaServiceConfiguration));
        }
    }
}