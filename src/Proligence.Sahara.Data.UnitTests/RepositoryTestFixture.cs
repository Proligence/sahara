﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RepositoryTestFixture.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Sahara.Data.UnitTests
{
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics.CodeAnalysis;
    using Autofac;
    using NUnit.Framework;
    using Proligence.Helpers.NHibernate;

    /// <summary>
    /// The base class for test fixtures which test data repositories for Sahara.
    /// </summary>
    /// <typeparam name="TRepositoryApi">The API of the tested repository.</typeparam>
    /// <typeparam name="TRepository">The type of the tested repository.</typeparam>
    public abstract class RepositoryTestFixture<TRepositoryApi, TRepository> 
        where TRepository : RepositoryBase
    {
        /// <summary>
        /// Gets the dependency injection container.
        /// </summary>
        protected IContainer Container { get; private set; }

        /// <summary>
        /// Gets the repository instance.
        /// </summary>
        protected TRepository Repository { get; private set; }

        /// <summary>
        /// Sets up the test fixture.
        /// </summary>
        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            ContainerBuilder builder = new ContainerBuilder();
            builder.RegisterModule(new TestSaharaDataModule());
            this.Container = builder.Build();

            this.Repository = this.Container.Resolve<TRepositoryApi>() as TRepository;
        }

        /// <summary>
        /// Tears down the test fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void FixtureTeardown()
        {
            this.Container.Dispose();
        }

        /// <summary>
        /// Executes the specified SQL query and returns the single value returned by the SQL server.
        /// </summary>
        /// <param name="query">The query to execute.</param>
        /// <param name="parameters">The query parameters.</param>
        /// <returns>The value returned by SQL server or <c>null</c> if no results were returned.</returns>
        [SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities",
            Justification = "It's ok here.")]
        protected object ExecuteQuery(string query, params SqlParameter[] parameters)
        {
            var connection = (SqlConnection)this.Repository.Connection;

            var command = connection.CreateCommand();
            command.CommandText = query;
            command.Parameters.AddRange(parameters);

            bool closeConnection = false;
            if (connection.State != ConnectionState.Open)
            {
                connection.Open();
                closeConnection = true;
            }

            object result = command.ExecuteScalar();

            if (closeConnection)
            {
                connection.Close();
            }

            return result;
        }
    }
}
