﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GrainRepositoryUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Sahara.Data.UnitTests
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Globalization;
    using System.Linq;
    using NUnit.Framework;
    using Proligence.Sahara.Client;

    /// <summary>
    /// Implements unit tests for the <see cref="GrainRepository"/> class.
    /// </summary>
    [TestFixture]
    [Category("Integration")]
    public class GrainRepositoryUnitTests : RepositoryTestFixture<IGrainRepository, GrainRepository>
    {
        /// <summary>
        /// Test grain instance.
        /// </summary>
        private Grain grain;

        /// <summary>
        /// Sets up the test fixture before each test.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.grain = new Grain 
                {
                    ApplicationName = "My app",
                    GrainType = "Info",
                    GrainMessage = "My message",
                    EventId = "My event"
                };
        }

        /// <summary>
        /// Tests if the <see cref="GrainRepository.Save"/> method correctly saves grains to the database.
        /// </summary>
        [Test]
        public void SaveGrainWithoutDataObject()
        {
            this.Repository.Save(this.grain);
            long grainId = this.grain.GrainId;

            Assert.That(this.GetGrainField("ApplicationName", grainId), Is.EqualTo(this.grain.ApplicationName));
            Assert.That(this.GetGrainField("GrainType", grainId), Is.EqualTo(this.grain.GrainType));
            Assert.That(this.GetGrainField("GrainMessage", grainId), Is.EqualTo(this.grain.GrainMessage));
            Assert.That(this.GetGrainField("EventId", grainId), Is.EqualTo(this.grain.EventId));
            Assert.That(this.GetGrainField("Source", grainId), Is.EqualTo(this.grain.Source));
            Assert.That(this.GetGrainField("UserName", grainId), Is.EqualTo(this.grain.UserName));
        }

        /// <summary>
        /// Tests if the <see cref="GrainRepository.Save"/> method correctly serializes grain data objects.
        /// </summary>
        [Test]
        public void SerializeGrainDataObject()
        {
            var exception = new InvalidOperationException("My exception message");
            this.grain.GrainData = new ExceptionGrainData(exception);

            this.Repository.Save(this.grain);
            long grainId = this.grain.GrainId;

            Assert.That(
                this.GetGrainData(grainId), 
                Is.EqualTo("{\"msg\":\"My exception message\",\"t\":\"InvalidOperationException\"}"));
        }

        /// <summary>
        /// Tests if the <see cref="GrainRepository.Save"/> method correctly saves and loads grains with a data 
        /// objects.
        /// </summary>
        [Test]
        public void SaveAndReadGrainWithDataObject()
        {
            var message = "My exception message";
            var exception = new InvalidOperationException(message);
            this.grain.GrainData = new ExceptionGrainData(exception);

            this.Repository.Save(this.grain);
            long grainId = this.grain.GrainId;

            var loadedGrain = this.Repository.GetById(grainId);
            Assert.That(((ExceptionGrainData)loadedGrain.GrainData).Message, Is.EqualTo(message));
        }

        /// <summary>
        /// Tests if the <see cref="GrainRepository.Save"/> method throws a <see cref="ArgumentNullException"/> if the
        /// specified argument is <c>null</c>.
        /// </summary>
        [Test]
        public void TrySaveNullGrain()
        {
            var exception = Assert.Throws<ArgumentNullException>(() => this.Repository.Save(null));
            
            Assert.That(
                exception.Message,
                Is.EqualTo("Value cannot be null." + Environment.NewLine + "Parameter name: obj"));
        }

        /// <summary>
        /// Tests if the <see cref="GrainRepository.Save"/> method throws a <see cref="InvalidOperationException"/> 
        /// when <see cref="Grain.ApplicationName"/> property set to <c>null</c>.
        /// </summary>
        [Test]
        [ExpectedException(typeof(InvalidOperationException),
            ExpectedMessage = "The grain's property ApplicationName must be set to a non-null value.")]
        public void TrySaveGrainWithApplicationNameNull()
        {
            this.grain.ApplicationName = null;
            this.Repository.Save(this.grain);
        }

        /// <summary>
        /// Tests if the <see cref="GrainRepository.Save"/> method throws a <see cref="InvalidOperationException"/> 
        /// when <see cref="Grain.EventId"/> property set to <c>null</c>.
        /// </summary>
        [Test]
        [ExpectedException(typeof(InvalidOperationException),
            ExpectedMessage = "The grain's property EventId must be set to a non-null value.")]
        public void TrySaveGrainWithEventIdNull()
        {
            this.grain.EventId = null;
            this.Repository.Save(this.grain);
        }

        /// <summary>
        /// Tests if the <see cref="GrainRepository.Save"/> method throws a <see cref="InvalidOperationException"/> 
        /// when <see cref="Grain.GrainMessage"/> property set to <c>null</c>.
        /// </summary>
        [Test]
        [ExpectedException(typeof(InvalidOperationException),
            ExpectedMessage = "The grain's property GrainMessage must be set to a non-null value.")]
        public void TrySaveGrainWithGrainMessageNull()
        {
            this.grain.GrainMessage = null;
            this.Repository.Save(this.grain);
        }

        /// <summary>
        /// Tests if the <see cref="GrainRepository.Save"/> method throws a <see cref="InvalidOperationException"/> 
        /// when <see cref="Grain.GrainType"/> property set to <c>null</c>.
        /// </summary>
        [Test]
        [ExpectedException(typeof(InvalidOperationException),
            ExpectedMessage = "The grain's property GrainType must be set to a non-null value.")]
        public void TrySaveGrainWithGrainTypeNull()
        {
            this.grain.GrainType = null;
            this.Repository.Save(this.grain);
        }

        /// <summary>
        /// Tests if the <see cref="GrainRepository.Save"/> method throws a <see cref="InvalidOperationException"/> 
        /// when <see cref="Grain.Source"/> property set to <c>null</c>.
        /// </summary>
        [Test]
        [ExpectedException(typeof(InvalidOperationException),
            ExpectedMessage = "The grain's property Source must be set to a non-null value.")]
        public void TrySaveGrainWithSourceNull()
        {
            this.grain.Source = null;
            this.Repository.Save(this.grain);
        }

        /// <summary>
        /// Tests if the <see cref="GrainRepository.Save"/> method throws a <see cref="InvalidOperationException"/> 
        /// when <see cref="Grain.UserName"/> property set to <c>null</c>.
        /// </summary>
        [Test]
        [ExpectedException(typeof(InvalidOperationException),
            ExpectedMessage = "The grain's property UserName must be set to a non-null value.")]
        public void TrySaveGrainWithUserNameNull()
        {
            this.grain.UserName = null;
            this.Repository.Save(this.grain);
        }

        /// <summary>
        /// Tests the <see cref="GrainRepository.GetById"/> method.
        /// </summary>
        [Test]
        public void GetGrainById()
        {
            this.Repository.Save(this.grain);
            long grainId = this.grain.GrainId;

            Grain loadedGrain = this.Repository.GetById(grainId);

            AssertEqual(this.grain, loadedGrain);
        }

        /// <summary>
        /// Tests if the <see cref="GrainRepository.GetByApplication"/> method throws a 
        /// <see cref="ArgumentNullException"/> when <c>null</c> is specified as the application name.
        /// </summary>
        [Test]
        public void GetGrainsByApplicationWhenApplicationNull()
        {
            var exception = Assert.Throws<ArgumentNullException>(
                () => this.Repository.GetByApplication(null));

            Assert.That(exception.ParamName, Is.EqualTo("applicationName"));
        }

        /// <summary>
        /// Tests if the <see cref="GrainRepository.GetByApplication"/> method returns grains of the specified 
        /// application.
        /// </summary>
        [Test]
        public void GetGrainByApplication()
        {
            this.Repository.Save(this.grain);
            IEnumerable<Grain> results = this.Repository.GetByApplication(this.grain.ApplicationName);

            Assert.That(results.Count(), Is.GreaterThan(0));

            foreach (var result in results) 
            {
                Assert.That(result.ApplicationName, Is.EqualTo(this.grain.ApplicationName));
            }
        }

        /// <summary>
        /// Tests if the <see cref="GrainRepository.GetByApplication"/> method returns grains of the specified 
        /// application and event id.
        /// </summary>
        [Test]
        public void GetGrainByApplicationAndEventId()
        {
            this.Repository.Save(this.grain);
            IEnumerable<Grain> results =
                this.Repository.GetByApplication(this.grain.ApplicationName, this.grain.EventId);

            Assert.That(results.Count(), Is.GreaterThan(0));

            foreach (var result in results) 
            {
                Assert.That(result.ApplicationName, Is.EqualTo(this.grain.ApplicationName));
                Assert.That(result.EventId, Is.LessThanOrEqualTo(this.grain.EventId));
            }
        }

        /// <summary>
        /// Tests if the <see cref="GrainRepository.GetByApplication"/> method properly returns grains when a maximum
        /// date is specified.
        /// </summary>
        [Test]
        public void GetGrainByApplicationAndMaximumDate()
        {
            this.grain.Timestamp = DateTime.Now.AddDays(-2);
            this.Repository.Save(this.grain);

            DateTime maximumDate = DateTime.Now.AddDays(-1);
            IEnumerable<Grain> results =
                this.Repository.GetByApplication(this.grain.ApplicationName, maximumDate: maximumDate);

            Assert.That(results.Count(), Is.GreaterThan(0));

            foreach (var result in results) 
            {
                Assert.That(result.ApplicationName, Is.EqualTo(this.grain.ApplicationName));
                Assert.That(result.Timestamp, Is.LessThanOrEqualTo(maximumDate));
            }
        }

        /// <summary>
        /// Tests if the <see cref="GrainRepository.GetByApplication"/> method correctly uses the it's <c>count</c>
        /// parameter.
        /// </summary>
        [Test]
        public void GetGrainByApplicationCount()
        {
            this.Repository.Save(this.grain);
            IEnumerable<Grain> results = this.Repository.GetByApplication(this.grain.ApplicationName, count: 1);

            Assert.That(results.Count(), Is.EqualTo(1));
        }

        /// <summary>
        /// Tests if the <see cref="GrainRepository.Delete(Proligence.Sahara.Client.Grain)"/> method deletes a single
        /// grain from the Sahara database.
        /// </summary>
        [Test]
        public void DeleteGrain()
        {
            this.Repository.Save(this.grain);
            int count = this.GetGrainCount();

            this.Repository.Delete(this.grain);
            int newCount = this.GetGrainCount();

            this.AssertGrainDoesNotExistsInDb(this.grain.GrainId);
            Assert.That(newCount, Is.EqualTo(count - 1));
        }

        /// <summary>
        /// Tests if the <see cref="GrainRepository.Delete(Proligence.Sahara.Client.Grain)"/> throws a 
        /// <see cref="ArgumentNullException"/> when the specified grain is <c>null</c>.
        /// </summary>
        [Test]
        public void DeleteGrainWhenArgumentNull()
        {
            var exception = Assert.Throws<ArgumentNullException>(() => this.Repository.Delete(null));
            Assert.That(exception.ParamName, Is.EqualTo("obj"));
        }

        /// <summary>
        /// Tests if the <see cref="GrainRepository.Delete(string,System.Nullable{System.DateTime})"/> correctly
        /// deletes grains of the specified application.
        /// </summary>
        [Test]
        public void DeleteGrainsByApplication()
        {
            string applicationName = Guid.NewGuid().ToString();
            
            var grain1 = new Grain
                {
                    ApplicationName = applicationName,
                    EventId = "My event",
                    GrainMessage = "My message",
                    GrainType = "My type"
                };
            this.Repository.Save(grain1);

            var grain2 = new Grain 
            {
                ApplicationName = applicationName,
                EventId = "My event",
                GrainMessage = "My message",
                GrainType = "My type"
            };
            this.Repository.Save(grain2);

            var grain3 = new Grain 
            {
                ApplicationName = applicationName,
                EventId = "My event",
                GrainMessage = "My message",
                GrainType = "My type"
            };
            this.Repository.Save(grain2);

            int count = this.GetGrainCount();

            this.Repository.Delete(applicationName);
            int newCount = this.GetGrainCount();

            this.AssertGrainDoesNotExistsInDb(grain1.GrainId);
            this.AssertGrainDoesNotExistsInDb(grain2.GrainId);
            this.AssertGrainDoesNotExistsInDb(grain3.GrainId);
            Assert.That(newCount, Is.EqualTo(count - 3));
        }

        /// <summary>
        /// Tests if the <see cref="GrainRepository.Delete(string,System.Nullable{System.DateTime})"/> correctly
        /// deletes grains of the specified application and before the specified date.
        /// </summary>
        [Test]
        public void DeleteGrainsByApplicationAndDate()
        {
            this.Repository.Save(this.grain);
            DateTime minDate = Convert.ToDateTime(
                this.ExecuteQuery("select min(Timestamp) from dbo.Grains"), CultureInfo.InvariantCulture);

            string applicationName = this.grain.ApplicationName;
            var olderGrain = new Grain 
            {
                ApplicationName = applicationName,
                EventId = "My event",
                GrainMessage = "My message",
                GrainType = "My type",
                Timestamp = minDate.AddDays(-2)
            };

            this.Repository.Save(olderGrain);
            int count = this.GetGrainCount();

            this.Repository.Delete(applicationName, minDate.AddDays(-1));
            int newCount = this.GetGrainCount();

            this.AssertGrainDoesNotExistsInDb(olderGrain.GrainId);
            Assert.That(newCount, Is.EqualTo(count - 1));
        }

        /// <summary>
        /// Asserts that two grains are equal.
        /// </summary>
        /// <param name="grain">The grain to compare.</param>
        /// <param name="other">The other grain to compare.</param>
        private static void AssertEqual(Grain grain, Grain other)
        {
            if ((grain != null) || (other != null)) 
            {
                Assert.That(grain, Is.Not.Null);
                Assert.That(other, Is.Not.Null);

                // ReSharper disable PossibleNullReferenceException
                Assert.That(grain.ApplicationName, Is.EqualTo(other.ApplicationName));
                // ReSharper restore PossibleNullReferenceException
                Assert.That(grain.EventId, Is.EqualTo(other.EventId));
                Assert.That(grain.GrainId, Is.EqualTo(other.GrainId));
                Assert.That(grain.GrainMessage, Is.EqualTo(other.GrainMessage));
                Assert.That(grain.GrainType, Is.EqualTo(other.GrainType));
                Assert.That(grain.Source, Is.EqualTo(other.Source));
                Assert.That(grain.Timestamp, Is.EqualTo(other.Timestamp).Within(new TimeSpan(0, 0, 1)));
                Assert.That(grain.UserName, Is.EqualTo(other.UserName));

                // Note: we don't check if the grain data is equal, as this is difficult to do generically.
                if (grain.GrainData != null) 
                {
                    Assert.That(other.GrainData, Is.Not.Null);
                }
                else 
                {
                    Assert.That(other.GrainData, Is.Null);
                }
            }
        }
        
        /// <summary>
        /// Gets the total count of grains in the database.
        /// </summary>
        /// <returns>The total count of grains in the database.</returns>
        private int GetGrainCount()
        {
            return (int)this.ExecuteQuery("select count(*) from dbo.Grains");
        }
        
        /// <summary>
        /// Asserts that a grain with the specified identifier does not exist in the database.
        /// </summary>
        /// <param name="grainId">The identifier of the grain.</param>
        private void AssertGrainDoesNotExistsInDb(long grainId)
        {
            object id = this.ExecuteQuery(
                "select * from dbo.Grains where GrainId = @GrainId",
                new SqlParameter("@GrainId", grainId));

            Assert.That(id, Is.Null, "Expected not to find grain with id " + grainId);
        }

        /// <summary>
        /// Reads the specified database field of the grain with the specified identifier.
        /// </summary>
        /// <param name="fieldName">Name of the field to read.</param>
        /// <param name="grainId">The grain identifier.</param>
        /// <returns>The string value saved in the field through the string table.</returns>
        private string GetGrainField(string fieldName, long grainId)
        {
            return (string)this.ExecuteQuery(
                "select st.Value " +
                "from Grains g " +
                "inner join StringTable st on st.StringId = g." + fieldName + " " +
                "where g.GrainId = @GrainId",
                new SqlParameter("@GrainId", grainId));
        }

        /// <summary>
        /// Gets the serialized data field of the grain with the specified identifier.
        /// </summary>
        /// <param name="grainId">The grain identifier.</param>
        /// <returns>The value of the grain's serialized data field.</returns>
        private string GetGrainData(long grainId)
        {
            return (string)this.ExecuteQuery(
                "select g.GrainData from Grains g where g.GrainId = @GrainId",
                new SqlParameter("@GrainId", grainId));
        }
    }
}