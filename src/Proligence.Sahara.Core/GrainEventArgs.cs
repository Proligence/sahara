﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GrainEventArgs.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Sahara.Core
{
    using System;
    using Proligence.Sahara.Client;

    /// <summary>
    /// Event arguments for grain-related events.
    /// </summary>
    internal class GrainEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GrainEventArgs"/> class.
        /// </summary>
        /// <param name="grain">The <see cref="Grain"/> to store in the event arguments.</param>
        public GrainEventArgs(Grain grain)
        {
            this.Grain = grain;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GrainEventArgs"/> class.
        /// </summary>
        /// <param name="grain">The <see cref="Grain"/> to store in the event arguments.</param>
        /// <param name="exception">The <see cref="Exception"/> to store in the event arguments.</param>
        public GrainEventArgs(Grain grain, Exception exception)
        {
            this.Grain = grain;
            this.Exception = exception;
        }

        /// <summary>
        /// Gets the grain related with the event.
        /// </summary>
        public Grain Grain { get; private set; }

        /// <summary>
        /// Gets the exception related with the event.
        /// </summary>
        public Exception Exception { get; private set; }
    }
}