﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GrainManager.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Sahara.Core
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Reflection;
    using System.Threading;
    using Proligence.Astoria.ServiceModel;
    using Proligence.Sahara.Client;
    using Proligence.Sahara.Configuration;
    using Proligence.Sahara.Data;

    /// <summary>
    /// Implements the Sahara Grain Manager service.
    /// </summary>
    public class GrainManager : AstoriaService<IGrainManager, SaharaConfiguration>, IGrainManager
    {
        /// <summary>
        /// Implements saving and reading grains from the database.
        /// </summary>
        private readonly IGrainRepository grainRepository;

        /// <summary>
        /// Level 1 save queue.
        /// </summary>
        private readonly SaveQueue level1Queue;

        /// <summary>
        /// Level 2 save queue.
        /// </summary>
        private readonly SaveQueue level2Queue;

        /// <summary>
        /// Level 3 save queue.
        /// </summary>
        private readonly SaveQueue level3Queue;

        /// <summary>
        /// Performance counters for the service.
        /// </summary>
        private readonly PerformanceCounters performanceCounters;

        /// <summary>
        /// Thread which updates non-event based performance counters.
        /// </summary>
        private readonly Thread performanceCounterUpdateThread;

        /// <summary>
        /// Initializes a new instance of the <see cref="GrainManager"/> class.
        /// </summary>
        /// <param name="grainRepository">Grain repository instance</param>
        /// <param name="configuration">The service's configuration</param>
        public GrainManager(IGrainRepository grainRepository, SaharaConfiguration configuration) 
            : base("Sahara Grain Manager", null, configuration)
        {
            this.grainRepository = grainRepository;

            this.performanceCounters = new PerformanceCounters();
            this.performanceCounters.Setup();

            this.level3Queue = this.CreateSaveQueue(
                this.Configuration.SaveQueues.Level3Interval,
                this.Configuration.SaveQueues.MaxQueueItems,
                null);

            this.level2Queue = this.CreateSaveQueue(
                this.Configuration.SaveQueues.Level2Interval,
                this.Configuration.SaveQueues.MaxQueueItems,
                this.level3Queue);

            this.level1Queue = this.CreateSaveQueue(
                this.Configuration.SaveQueues.Level1Interval,
                this.Configuration.SaveQueues.MaxQueueItems,
                this.level2Queue);

            this.performanceCounterUpdateThread = new Thread(this.PerformanceCounterUpdateThread) 
            {
                Name = "Sahara Performance Counter Update Thread",
                IsBackground = true
            };

            this.performanceCounterUpdateThread.Start();
        }

        /// <summary>
        /// Stores the specified <see cref="Grain"/> in Sahara.
        /// </summary>
        /// <param name="grain">The <see cref="Grain"/> object which represents the grain to save to Sahara.</param>
        public void SaveGrain(Grain grain)
        {
            this.WithLog(
                () => this.level1Queue.AddToQueue(grain), logSuccess: false);
        }

        /// <summary>
        /// Gets the specified grains.
        /// </summary>
        /// <param name="applicationName">The name of the application for which grains will be returned.</param>
        /// <param name="count">The maximum number of grains to get.</param>
        /// <returns>A sequence of <see cref="Grain"/> objects which represent the result grains.</returns>
        public IEnumerable<Grain> GetGrains(string applicationName, int count)
        {
            return this.WithLog(
                () => this.grainRepository.GetByApplication(applicationName, count: count));
        }

        /// <summary>
        /// Gets the specified grains.
        /// </summary>
        /// <param name="applicationName">The name of the application for which grains will be returned.</param>
        /// <param name="eventId">The event identifier of the grains to get.</param>
        /// <param name="count">The maximum number of grains to get.</param>
        /// <returns>A sequence of <see cref="Grain"/> objects which represent the result grains.</returns>
        public IEnumerable<Grain> GetGrains(string applicationName, string eventId, int count)
        {
            return this.WithLog(
                () => this.grainRepository.GetByApplication(applicationName, eventId, count: count));
        }

        /// <summary>
        /// Gets the specified grains.
        /// </summary>
        /// <param name="applicationName">The name of the application for which grains will be returned.</param>
        /// <param name="maximumDate">The maximum date of to grains to get.</param>
        /// <returns>A sequence of <see cref="Grain"/> objects which represent the result grains.</returns>
        public IEnumerable<Grain> GetGrains(string applicationName, DateTime maximumDate)
        {
            return this.WithLog(
                () => this.grainRepository.GetByApplication(applicationName, maximumDate: maximumDate));
        }

        /// <summary>
        /// Gets the specified grains.
        /// </summary>
        /// <param name="applicationName">The name of the application for which grains will be returned.</param>
        /// <param name="eventId">The event identifier of the grains to get.</param>
        /// <param name="maximumDate">The maximum date of to grains to get.</param>
        /// <returns>A sequence of <see cref="Grain"/> objects which represent the result grains.</returns>
        public IEnumerable<Grain> GetGrains(string applicationName, string eventId, DateTime maximumDate)
        {
            return this.WithLog(
                () => this.grainRepository.GetByApplication(applicationName, eventId, maximumDate));
        }

        /// <summary>
        /// Removes all grains of the specified application.
        /// </summary>
        /// <param name="applicationName">The name of the application which grains will be deleted.</param>
        public void DeleteGrains(string applicationName)
        {
            this.WithLog(
                () => this.grainRepository.Delete(applicationName));
        }

        /// <summary>
        /// Removes all grains of the specified application.
        /// </summary>
        /// <param name="applicationName">The name of the application which grains will be deleted.</param>
        /// <param name="maximumDate">The maximum date of to grains to delete.</param>
        public void DeleteGrains(string applicationName, DateTime maximumDate)
        {
            this.WithLog(
                () => this.grainRepository.Delete(applicationName, maximumDate));
        }

        /// <summary>
        /// Formats an error message with the details of the specified exception.
        /// </summary>
        /// <param name="message">The error message.</param>
        /// <param name="exception">The exception object.</param>
        /// <returns>The formatted error message.</returns>
        private static string FormatError(string message, Exception exception)
        {
            string str = message.Replace(Environment.NewLine, string.Empty);
            
            if (!message.EndsWith(" ", StringComparison.Ordinal)) 
            {
                str += message + " ";
            }
            else
            {
                str += message;
            }

            while (exception != null)
            {
                if (!str.Contains(exception.Message.Trim()))
                {
                    if (!exception.Message.EndsWith(" ", StringComparison.Ordinal))
                    {
                        str += exception.Message + " ";
                    }
                    else 
                    {
                        str += exception.Message;
                    }   
                }

                exception = exception.InnerException;
            }

            return str;
        }

        /// <summary>
        /// Handles the <see cref="SaveQueue.GrainSaved"/> event.
        /// </summary>
        /// <param name="sender">The save queue which triggered this event.</param>
        /// <param name="e">The <see cref="GrainEventArgs"/> instance containing the event data.</param>
        private void OnGrainSaved(object sender, GrainEventArgs e)
        {
            this.performanceCounters.SavedGrainsPc.Increment();
        }

        /// <summary>
        /// Handles the <see cref="SaveQueue.GrainFailed"/> event.
        /// </summary>
        /// <param name="sender">The save queue which triggered this event.</param>
        /// <param name="e">The <see cref="GrainEventArgs"/> instance containing the event data.</param>
        private void OnGrainFailed(object sender, GrainEventArgs e)
        {
            this.performanceCounters.FailedGrainsPc.Increment();
            this.LogError(FormatError("Failed to save grain.", e.Exception), e.Exception);
        }

        /// <summary>
        /// Handles the <see cref="SaveQueue.GrainDiscarded"/> event.
        /// </summary>
        /// <param name="sender">The save queue which triggered this event.</param>
        /// <param name="e">The <see cref="GrainEventArgs"/> instance containing the event data.</param>
        private void OnGrainDiscarded(object sender, GrainEventArgs e)
        {
            this.performanceCounters.DiscardedGrainsPc.Increment();
            this.LogError(FormatError("Grain discarded.", e.Exception), e.Exception);
        }

        /// <summary>
        /// Creates a new save queue.
        /// </summary>
        /// <param name="interval">The queue's processing interval.</param>
        /// <param name="maximumCount">The maximum size of the queue.</param>
        /// <param name="nextQueue">The next queue.</param>
        /// <returns>The created save queue.</returns>
        private SaveQueue CreateSaveQueue(int interval, int maximumCount, SaveQueue nextQueue)
        {
            var saveQueue = new SaveQueue(this.grainRepository) 
            {
                ProcessInterval = interval,
                MaximumCount = maximumCount,
                NextQueue = nextQueue
            };

            saveQueue.GrainSaved += this.OnGrainSaved;
            saveQueue.GrainFailed += this.OnGrainFailed;
            saveQueue.GrainDiscarded += this.OnGrainDiscarded;

            return saveQueue;
        }

        /// <summary>
        /// Updates non-event based performance counters.
        /// </summary>
        private void PerformanceCounterUpdateThread()
        {
            while (true) 
            {
                this.performanceCounters.Level1GrainsPc.RawValue = this.level1Queue.CurrentCount;
                this.performanceCounters.Level2GrainsPc.RawValue = this.level2Queue.CurrentCount;
                this.performanceCounters.Level3GrainsPc.RawValue = this.level3Queue.CurrentCount;
                Thread.Sleep(1000);
            }
        }

        /// <summary>
        /// Executes the specified action and writes an entry to the service's log.
        /// </summary>
        /// <param name="action">Action to invoke.</param>
        /// <param name="logSuccess">
        /// If set to <c>true</c> succeeded calls will be logged; otherwise only failed calls will be logged.
        /// </param>
        private void WithLog(Action action, bool logSuccess = true)
        {
            MethodBase callingMethod = new StackFrame(1).GetMethod();
            try 
            {
                action();

                if (logSuccess)
                {
                    this.LogMessage(callingMethod.Name + ": call succeeded");   
                }
            }
            catch (Exception ex) 
            {
                this.LogError(FormatError(callingMethod.Name + ": call failed", ex), ex);
                throw;
            }
        }

        /// <summary>
        /// Executes the specified function and writes an entry to the service's log.
        /// </summary>
        /// <typeparam name="T">The type of the function's result.</typeparam>
        /// <param name="func">Function to invoke.</param>
        /// <param name="logSuccess">
        /// If set to <c>true</c> succeeded calls will be logged; otherwise only failed calls will be logged.
        /// </param>
        /// <returns>The result of the invoked function.</returns>
        private T WithLog<T>(Func<T> func, bool logSuccess = true)
        {
            MethodBase callingMethod = new StackFrame(1).GetMethod();
            try 
            {
                T result = func();

                if (logSuccess)
                {
                    this.LogMessage(callingMethod.Name + ": call succeeded");   
                }

                return result;
            }
            catch (Exception ex) 
            {
                this.LogError(FormatError(callingMethod.Name + ": call failed", ex), ex);
                throw;
            }
        }
    }
}
