﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PerformanceCounters.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Sahara.Core
{
    using System.Diagnostics;

    /// <summary>
    /// Manages performance counters for the Sahara Grain Manager service.
    /// </summary>
    internal class PerformanceCounters
    {
        /// <summary>
        /// The name of the category for the performance counters.
        /// </summary>
        private const string CategoryName = "Sahara Grain Manager";

        /// <summary>
        /// Performance counter for the number of currently queued grains.
        /// </summary>
        private PerformanceCounter level1GrainsPc;

        /// <summary>
        /// Performance counter for the number of currently queued grains.
        /// </summary>
        private PerformanceCounter level2GrainsPc;

        /// <summary>
        /// Performance counter for the number of currently queued grains.
        /// </summary>
        private PerformanceCounter level3GrainsPc;

        /// <summary>
        /// Performance counter for the total number of saved grains.
        /// </summary>
        private PerformanceCounter savedGrainsPc;

        /// <summary>
        /// Performance counter for the total number of failed grains.
        /// </summary>
        private PerformanceCounter failedGrainsPc;

        /// <summary>
        /// Performance counter for the total number of discarded grains.
        /// </summary>
        private PerformanceCounter discardedGrainsPc;

        /// <summary>
        /// Gets the performance counter for the number of currently queued grains.
        /// </summary>
        public PerformanceCounter Level1GrainsPc
        {
            get { return this.level1GrainsPc; }
        }

        /// <summary>
        /// Gets the performance counter for the number of currently queued grains.
        /// </summary>
        public PerformanceCounter Level2GrainsPc
        {
            get { return this.level2GrainsPc; }
        }

        /// <summary>
        /// Gets the performance counter for the number of currently queued grains.
        /// </summary>
        public PerformanceCounter Level3GrainsPc
        {
            get { return this.level3GrainsPc; }
        }

        /// <summary>
        /// Gets the performance counter for the total number of saved grains.
        /// </summary>
        public PerformanceCounter SavedGrainsPc
        {
            get { return this.savedGrainsPc; }
        }

        /// <summary>
        /// Gets the performance counter for the total number of failed grains.
        /// </summary>
        public PerformanceCounter FailedGrainsPc
        {
            get { return this.failedGrainsPc; }
        }

        /// <summary>
        /// Gets the performance counter for the total number of discarded grains.
        /// </summary>
        public PerformanceCounter DiscardedGrainsPc
        {
            get { return this.discardedGrainsPc; }
        }

        /// <summary>
        /// Sets up the performance counters and categories.
        /// </summary>
        public void Setup()
        {
            if (!PerformanceCounterCategory.Exists(CategoryName))
            {
                var counterDataCollection = new CounterCreationDataCollection();

                counterDataCollection.Add(
                    new CounterCreationData
                        {
                            CounterName = "Level 1 Queued Grains", 
                            CounterType = PerformanceCounterType.NumberOfItems32
                        });

                counterDataCollection.Add(
                    new CounterCreationData
                        {
                            CounterName = "Level 2 Queued Grains",
                            CounterType = PerformanceCounterType.NumberOfItems32
                        });

                counterDataCollection.Add(
                    new CounterCreationData 
                        {
                            CounterName = "Level 3 Queued Grains",
                            CounterType = PerformanceCounterType.NumberOfItems32
                        });

                counterDataCollection.Add(
                    new CounterCreationData 
                        {
                            CounterName = "Saved Grains",
                            CounterType = PerformanceCounterType.NumberOfItems64
                        });

                counterDataCollection.Add(
                    new CounterCreationData 
                        {
                            CounterName = "Failed Grains",
                            CounterType = PerformanceCounterType.NumberOfItems64
                        });

                counterDataCollection.Add(
                    new CounterCreationData 
                        {
                            CounterName = "Discarded Grains",
                            CounterType = PerformanceCounterType.NumberOfItems64
                        });

                PerformanceCounterCategory.Create(
                    CategoryName,
                    "Performance counters for the Sahara Grain Manager service.",
                    PerformanceCounterCategoryType.SingleInstance,
                    counterDataCollection);
            }

            this.level1GrainsPc = new PerformanceCounter(CategoryName, "Level 1 Queued Grains", false)
                {
                    RawValue = 0
                };

            this.level2GrainsPc = new PerformanceCounter(CategoryName, "Level 2 Queued Grains", false) 
                { 
                    RawValue = 0 
                };

            this.level3GrainsPc = new PerformanceCounter(CategoryName, "Level 3 Queued Grains", false)
                {
                    RawValue = 0
                };

            this.savedGrainsPc = new PerformanceCounter(CategoryName, "Saved Grains", false)
                {
                    RawValue = 0
                };

            this.failedGrainsPc = new PerformanceCounter(CategoryName, "Failed Grains", false)
                {
                    RawValue = 0
                };

            this.discardedGrainsPc = new PerformanceCounter(CategoryName, "Discarded Grains", false)
                {
                    RawValue = 0
                };
        }
    }
}
