﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SaveQueue.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Sahara.Core
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Threading;
    using Proligence.Sahara.Client;
    using Proligence.Sahara.Data;

    /// <summary>
    /// Queues and saves Sahara grains.
    /// </summary>
    internal class SaveQueue
    {
        /// <summary>
        /// Grain repository.
        /// </summary>
        private readonly IGrainRepository grainRepository;

        /// <summary>
        /// Queues grain which will be saved to the Sahara database.
        /// </summary>
        private readonly Queue<Grain> grainQueue;

        /// <summary>
        /// The thread which processes grains in the queue.
        /// </summary>
        private readonly Thread saveQueueThread;

        /// <summary>
        /// Specifies if the save queue thread is running.
        /// </summary>
        private bool saveQueueThreadRunning;

        /// <summary>
        /// Initializes a new instance of the <see cref="SaveQueue"/> class.
        /// </summary>
        /// <param name="grainRepository">The grain repository.</param>
        public SaveQueue(IGrainRepository grainRepository)
        {
            this.grainRepository = grainRepository;
            this.grainQueue = new Queue<Grain>();
            this.ProcessInterval = 0;
            this.MaximumCount = 1000;

            this.saveQueueThread = new Thread(this.SaveQueueThread)
                { 
                    Name = "Sahara Save Queue Thread", 
                    IsBackground = true 
                };

            this.saveQueueThread.Start();
            this.saveQueueThreadRunning = true;
        }

        /// <summary>
        /// Occurs when a grain is saved to Sahara database.
        /// </summary>
        public event EventHandler<GrainEventArgs> GrainSaved;

        /// <summary>
        /// Occurs when a grain is moved to the next queue due to an error.
        /// </summary>
        public event EventHandler<GrainEventArgs> GrainFailed;

        /// <summary>
        /// Occurs when a grain is discarded due to an error if there is no next queue.
        /// </summary>
        public event EventHandler<GrainEventArgs> GrainDiscarded;

        /// <summary>
        /// Gets or sets the queue which will process grains which failed to save on this queue.
        /// </summary>
        public SaveQueue NextQueue { get; set; }

        /// <summary>
        /// Gets or sets the number of milliseconds which the queue will wait between saving grains.
        /// </summary>
        public int ProcessInterval { get; set; }

        /// <summary>
        /// Gets or sets the number of items above which grains will be discarded (removed from the queue).
        /// </summary>
        public int MaximumCount { get; set; }

        /// <summary>
        /// Gets the number current number of items in the queue.
        /// </summary>
        public int CurrentCount { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the queue is full.
        /// </summary>
        public bool IsFull
        {
            get
            {
                return this.CurrentCount >= this.MaximumCount;
            }
        }

        /// <summary>
        /// Adds the specified grain to the save queue.
        /// </summary>
        /// <param name="grain">The grain to add to the queue.</param>
        public void AddToQueue(Grain grain)
        {
            lock (this.grainQueue)
            {
                if (this.IsFull)
                {
                    if ((this.NextQueue != null) && !this.NextQueue.IsFull)
                    {
                        this.NextQueue.AddToQueue(grain);
                    }
                    else
                    {
                        this.OnGrainDiscarded(new GrainEventArgs(grain, null));
                    }
                }
                else
                {
                    this.grainQueue.Enqueue(grain);
                }

                this.CurrentCount = this.grainQueue.Count;
            }
        }

        /// <summary>
        /// Stops grain processing on the queue.
        /// </summary>
        public void Close()
        {
            this.saveQueueThreadRunning = false;
        }

        /// <summary>
        /// Suspends the save queue thread for the specified amount of time.
        /// </summary>
        /// <param name="milliseconds">The number of milliseconds to wait.</param>
        protected virtual void Wait(int milliseconds)
        {
            if (milliseconds > 0)
            {
                Thread.Sleep(milliseconds);   
            }
        }

        /// <summary>
        /// Raises the <see cref="GrainSaved"/> event.
        /// </summary>
        /// <param name="e">The <see cref="GrainEventArgs"/> instance containing the event data.</param>
        protected virtual void OnGrainSaved(GrainEventArgs e)
        {
            EventHandler<GrainEventArgs> handler = this.GrainSaved;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Raises the <see cref="GrainFailed"/> event.
        /// </summary>
        /// <param name="e">The <see cref="GrainEventArgs"/> instance containing the event data.</param>
        protected virtual void OnGrainFailed(GrainEventArgs e)
        {
            EventHandler<GrainEventArgs> handler = this.GrainFailed;
            if (handler != null) 
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Raises the <see cref="GrainDiscarded"/> event.
        /// </summary>
        /// <param name="e">The <see cref="GrainEventArgs"/> instance containing the event data.</param>
        protected virtual void OnGrainDiscarded(GrainEventArgs e)
        {
            EventHandler<GrainEventArgs> handler = this.GrainDiscarded;
            if (handler != null) 
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Processes grains in the queue.
        /// </summary>
        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "By design.")]
        private void SaveQueueThread()
        {
            while (this.saveQueueThreadRunning)
            {
                Grain grain = null;
                lock (this.grainQueue)
                {
                    if (this.grainQueue.Count > 0)
                    {
                        grain = this.grainQueue.Dequeue();
                    }
                }

                try 
                {
                    if (grain != null)
                    {
                        this.grainRepository.Save(grain);
                        this.OnGrainSaved(new GrainEventArgs(grain));
                    }
                }
                catch (Exception ex)
                {
                    if ((this.NextQueue != null) && !this.NextQueue.IsFull)
                    {
                        this.OnGrainFailed(new GrainEventArgs(grain, ex));
                        this.NextQueue.AddToQueue(grain);
                    }
                    else
                    {
                        if (this.IsFull)
                        {
                            this.OnGrainDiscarded(new GrainEventArgs(grain, ex));
                        }
                        else
                        {
                            this.OnGrainFailed(new GrainEventArgs(grain, ex));
                            this.grainQueue.Enqueue(grain);
                        }
                    }
                }

                this.CurrentCount = this.grainQueue.Count;
                this.Wait(this.ProcessInterval);
            }
        }
    }
}