﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ServiceDeploymentTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Sahara.Core.UnitTests
{
    using System;
    using System.Diagnostics;
    using System.Threading;
    using Autofac;
    using NUnit.Framework;
    using Proligence.Astoria.Client;
    using Proligence.Sahara.Client;

    /// <summary>
    /// Implements post-deployment tests for the service.
    /// </summary>
    [TestFixture, Explicit]
    public class ServiceDeploymentTests
    {
        /// <summary>
        /// Creates a single grain in Sahara.
        /// </summary>
        [Test]
        public void CreateSingleGrain()
        {
            var grain = new Grain 
            {
                ApplicationName = "Test",
                EventId = "TestEventId",
                GrainMessage = "This is a test grain.",
                GrainType = "Information",
                Source = "TestSource"
            };

            CreateGrains(grain, 0);
        }

        /// <summary>
        /// Tests performance of creating simple grains in Sahara.
        /// </summary>
        [Test]
        public void CreateSimpleGrains()
        {
            var grain = new Grain 
            {
                ApplicationName = "Test",
                EventId = "TestEventId",
                GrainMessage = "This is a test grain.",
                GrainType = "Information",
                Source = "TestSource"
            };

            CreateGrains(grain, 1000);
        }

        /// <summary>
        /// Slowly creates simple grains in Sahara.
        /// </summary>
        [Test]
        public void CreateGrainsSlowly()
        {
            var grain = new Grain 
            {
                ApplicationName = "Test",
                EventId = "TestEventId",
                GrainMessage = "This is a test grain.",
                GrainType = "Information",
                Source = "TestSource"
            };

            CreateGrains(grain, 100, 1000);
        }

        /// <summary>
        /// Tests performance of creating grains with data objects in Sahara.
        /// </summary>
        [Test]
        public void CreateGrainsWithDataObjects()
        {
            Exception exception = null;
            try
            {
                int a = 2, b = 0;
                Trace.WriteLine(a / b);
            }
            catch (DivideByZeroException ex)
            {
                exception = ex;
            }

            string serviceUrl = "net.tcp://localhost:81/Proligence/Sahara/GrainManager/ast";

            ContainerBuilder builder = new ContainerBuilder();
            builder.RegisterModule(new AstoriaClientModule());
            using (var container = builder.Build())
            {
                IAstoriaChannelFactory channelFactory = container.Resolve<IAstoriaChannelFactory>();
                IGrainManager grainManager = channelFactory.CreateChannel<IGrainManager>(serviceUrl);

                long ticks = Environment.TickCount;

                for (int i = 0; i < 1000; i++)
                {
                    var grain = new Grain 
                    {
                        ApplicationName = "Test",
                        EventId = "TestEventId",
                        GrainMessage = "This is a test grain.",
                        GrainType = "Information",
                        Source = "TestSource",
                        GrainData = new ExceptionGrainData(exception)
                    };

                    grainManager.SaveGrain(grain);
                }

                ticks = Environment.TickCount - ticks;

                Trace.WriteLine("Total time: " + ticks + " ms");
                Trace.WriteLine("Avg time: " + ((float)ticks / 1000) + " ms");
            }
        }

        /// <summary>
        /// Creates the specified grains many times in Sahara.
        /// </summary>
        /// <param name="grain">The grain to create.</param>
        /// <param name="count">The number of grains to create.</param>
        /// <param name="interval">The number of milliseconds to wait between creating grains.</param>
        private static void CreateGrains(Grain grain, int count, int interval = 0)
        {
            string serviceUrl = "net.tcp://localhost:81/Proligence/Sahara/GrainManager/ast";

            ContainerBuilder builder = new ContainerBuilder();
            builder.RegisterModule(new AstoriaClientModule());
            using (var container = builder.Build()) 
            {
                IAstoriaChannelFactory channelFactory = container.Resolve<IAstoriaChannelFactory>();
                IGrainManager grainManager = channelFactory.CreateChannel<IGrainManager>(serviceUrl);

                grainManager.SaveGrain(grain);

                long ticks = Environment.TickCount;

                for (int i = 0; i < count; i++) 
                {
                    grainManager.SaveGrain(grain);

                    if (interval > 0)
                    {
                        Thread.Sleep(interval);
                    }
                }

                ticks = Environment.TickCount - ticks;

                Trace.WriteLine("Total time: " + ticks + " ms");
                Trace.WriteLine("Avg time: " + ((float)ticks / count) + " ms");
            }
        }
    }
}