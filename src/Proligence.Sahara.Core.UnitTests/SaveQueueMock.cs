﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SaveQueueMock.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Sahara.Core.UnitTests
{
    using System.Diagnostics.CodeAnalysis;
    using System.Threading;
    using Proligence.Sahara.Data;

    /// <summary>
    /// Mocks the <see cref="SaveQueue"/> class for testing purposes.
    /// </summary>
    [SuppressMessage("Microsoft.Design", "CA1001:TypesThatOwnDisposableFieldsShouldBeDisposable",
        Justification = "Test code")]
    internal class SaveQueueMock : SaveQueue
    {
        /// <summary>
        /// Wait handle for blocking current thread in <see cref="Wait"/> method.
        /// </summary>
        private readonly EventWaitHandle waitHandle;

        /// <summary>
        /// Initializes a new instance of the <see cref="SaveQueueMock"/> class.
        /// </summary>
        /// <param name="grainRepository">The grain repository.</param>
        public SaveQueueMock(IGrainRepository grainRepository)
            : base(grainRepository)
        {
            this.waitHandle = new EventWaitHandle(false, EventResetMode.AutoReset);
            Thread.Sleep(100);   // wait for background thread initialization
        }

        /// <summary>
        /// Releases the thread blocked in <see cref="Wait"/> method.
        /// </summary>
        public void EndWait()
        {
            this.waitHandle.Set();
        }

        /// <summary>
        /// Suspends the save queue thread for the specified amount of time.
        /// </summary>
        /// <param name="milliseconds">The number of milliseconds to wait.</param>
        protected override void Wait(int milliseconds)
        {
            this.waitHandle.WaitOne(1000);
        }
    }
}