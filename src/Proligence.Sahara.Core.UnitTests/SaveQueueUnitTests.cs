﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SaveQueueUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Sahara.Core.UnitTests
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.Threading;
    using Moq;
    using NUnit.Framework;
    using Proligence.Sahara.Client;
    using Proligence.Sahara.Data;

    /// <summary>
    /// Implements unit tests for the <see cref="SaveQueue"/> class.
    /// </summary>
    [TestFixture]
    [SuppressMessage("Microsoft.Design", "CA1001:TypesThatOwnDisposableFieldsShouldBeDisposable", 
        Justification = "Test code")]
    [Timeout(10000)]
    [Explicit("Deadlocks occur here... must figure out how to implement these tests better!")]
    public class SaveQueueUnitTests
    {
        /// <summary>
        /// Mock for the grain repository.
        /// </summary>
        private Mock<IGrainRepository> grainRepositoryMock;

        /// <summary>
        /// Wait handle which is set when a grain is saved.
        /// </summary>
        private EventWaitHandle saveWaitHandle;

        /// <summary>
        /// The grain which was last saved by the grain repository.
        /// </summary>
        private Grain savedGrain;

        /// <summary>
        /// The tested <see cref="SaveQueue"/> instance.
        /// </summary>
        private SaveQueueMock saveQueue;

        /// <summary>
        /// Sets up the test fixture before each test.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.saveWaitHandle = new EventWaitHandle(false, EventResetMode.AutoReset);

            this.grainRepositoryMock = new Mock<IGrainRepository>();
            this.grainRepositoryMock.Setup(x => x.Save(It.IsAny<Grain>())).Callback(
                new Action<Grain>(
                    g =>
                        {
                            this.savedGrain = g;
                            this.saveWaitHandle.Set();
                        }));

            this.saveQueue = new SaveQueueMock(this.grainRepositoryMock.Object);
        }

        /// <summary>
        /// Tears down the test fixture.
        /// </summary>
        [TearDown]
        public void Teardown()
        {
            if (this.saveQueue != null)
            {
                this.saveQueue.EndWait();
                this.saveQueue.Close();
            }

            this.saveWaitHandle.Dispose();
        }

        /// <summary>
        /// Tests if a single grain is processing correctly in the queue.
        /// </summary>
        [Test]
        public void ProcessGrainInQueue()
        {
            Grain grain = CreateSampleGrain();
            this.saveQueue.AddToQueue(grain);
            this.saveQueue.EndWait();
            this.WaitForSave();
            
            Assert.That(this.savedGrain, Is.SameAs(grain));
        }

        /// <summary>
        /// Tests if a grain saved to a full queue is moved to the next queue if the next queue is not full.
        /// </summary>
        [Test]
        public void AddToQueueWhenCurrentQueueFullAndNextQueueNotFull()
        {
            using (var waitHandle = new EventWaitHandle(false, EventResetMode.AutoReset))
            {
                Grain saved = null;

                var grainRepository = new Mock<IGrainRepository>();
                grainRepository.Setup(x => x.Save(It.IsAny<Grain>())).Callback(
                    new Action<Grain>(
                        g => 
                        {
                            saved = g;
                            waitHandle.Set();
                        }));

                var nextQueue = new SaveQueueMock(grainRepository.Object) 
                {
                    MaximumCount = 1
                };

                var queue = new SaveQueueMock(this.grainRepositoryMock.Object)
                {
                    MaximumCount = 0,
                    NextQueue = nextQueue
                };

                Grain grain = CreateSampleGrain();
                queue.AddToQueue(grain);

                queue.EndWait();
                nextQueue.EndWait();
                waitHandle.WaitOne();
                
                Assert.That(saved, Is.EqualTo(grain));

                queue.EndWait();
                nextQueue.EndWait();
            }
        }

        /// <summary>
        /// Tests if a grain saved to a full queue is discarded if there is no next queue.
        /// </summary>
        [Test]
        public void AddToQueueWhenCurrentQueueFullAndNoNextQueue()
        {
            using (var waitHandle = new EventWaitHandle(false, EventResetMode.AutoReset)) 
            {
                var queue = new SaveQueueMock(this.grainRepositoryMock.Object)
                {
                    MaximumCount = 0,
                    NextQueue = null
                };
                
                GrainEventArgs eventArgs = null;
                queue.GrainDiscarded += (sender, e) => 
                {
                    eventArgs = e;
                    waitHandle.Set();
                };

                Grain grain = CreateSampleGrain();
                queue.AddToQueue(grain);

                queue.EndWait();
                waitHandle.WaitOne();

                AssertGrainEventArgs(eventArgs, grain, null);
                
                queue.EndWait();
            }
        }

        /// <summary>
        /// Tests if a grain saved to a full queue is discarded if the next queue is full.
        /// </summary>
        [Test]
        public void AddToQueueWhenCurrentQueueFullAndNextQueueFull()
        {
            using (var waitHandle = new EventWaitHandle(false, EventResetMode.AutoReset))
            {
                var queue = new SaveQueueMock(this.grainRepositoryMock.Object)
                {
                    MaximumCount = 0,
                    NextQueue = new SaveQueueMock(this.grainRepositoryMock.Object)
                    {
                        MaximumCount = 0
                    }
                };

                GrainEventArgs eventArgs = null;
                queue.GrainDiscarded += (sender, e) =>
                {
                    eventArgs = e;
                    waitHandle.Set();
                };

                Grain grain = CreateSampleGrain();
                queue.AddToQueue(grain);

                queue.EndWait();
                waitHandle.WaitOne();

                AssertGrainEventArgs(eventArgs, grain, null);

                queue.EndWait();
            }
        }

        /// <summary>
        /// Tests if multiple grains are processed correctly in the queue.
        /// </summary>
        [Test]
        public void ProcessMultipleGrainsInQueue()
        {
            Grain grain1 = CreateSampleGrain();
            Grain grain2 = CreateSampleGrain();
            Grain grain3 = CreateSampleGrain();

            this.saveQueue.AddToQueue(grain1);
            this.saveQueue.AddToQueue(grain2);
            this.saveQueue.AddToQueue(grain3);

            this.saveQueue.EndWait();
            this.WaitForSave();
            Assert.That(this.savedGrain, Is.SameAs(grain1));

            this.saveQueue.EndWait();
            this.WaitForSave();
            Assert.That(this.savedGrain, Is.EqualTo(grain2));

            this.saveQueue.EndWait();
            this.WaitForSave();
            Assert.That(this.savedGrain, Is.EqualTo(grain3));
        }

        /// <summary>
        /// Tests if the <see cref="SaveQueue.GrainSaved"/> event is called after saving a grain.
        /// </summary>
        [Test]
        public void GrainSavedEvent()
        {
            using (var waitHandle = new EventWaitHandle(false, EventResetMode.AutoReset))
            {
                GrainEventArgs eventArgs = null;
                this.saveQueue.GrainSaved += (sender, e) =>
                    {
                        eventArgs = e;
                        waitHandle.Set();
                    };

                Grain grain = CreateSampleGrain();
                this.saveQueue.AddToQueue(grain);
                this.saveQueue.EndWait();
                this.WaitForSave();
                waitHandle.WaitOne();

                AssertGrainEventArgs(eventArgs, grain, null);
            }
        }

        /// <summary>
        /// Tests if the <see cref="SaveQueue.GrainFailed"/> event is called if an error occurs while saving a grain.
        /// </summary>
        [Test]
        public void GrainFailedEvent()
        {
            this.grainRepositoryMock.Setup(x => x.Save(It.IsAny<Grain>())).Callback(
                new Action<Grain>(
                    g =>
                        {
                            throw new InvalidOperationException("Test");
                        }));

            this.saveQueue.NextQueue = new SaveQueueMock(this.grainRepositoryMock.Object);

            using (var waitHandle = new EventWaitHandle(false, EventResetMode.AutoReset))
            {
                GrainEventArgs eventArgs = null;
                this.saveQueue.GrainFailed += (sender, e) =>
                    {
                        eventArgs = e;
                        waitHandle.Set();
                    };

                Grain grain = CreateSampleGrain();
                this.saveQueue.AddToQueue(grain);
                this.saveQueue.EndWait();
                waitHandle.WaitOne();

                AssertGrainEventArgs(eventArgs, grain, "Test");
            }
        }

        /// <summary>
        /// Tests if the <see cref="SaveQueue.GrainFailed"/> event is called if an error occurs while saving a grain.
        /// </summary>
        [Test]
        public void GrainDiscardedEvent()
        {
            this.grainRepositoryMock.Setup(x => x.Save(It.IsAny<Grain>())).Callback(
                new Action<Grain>(
                    g =>
                        { 
                            throw new InvalidOperationException("Test"); 
                        }));

            this.saveQueue.MaximumCount = 1;

            using (var waitHandle = new EventWaitHandle(false, EventResetMode.AutoReset)) 
            {
                GrainEventArgs eventArgs = null;
                this.saveQueue.GrainDiscarded += (sender, e) =>
                    {
                        eventArgs = e;
                        waitHandle.Set();
                    };

                Grain grain = CreateSampleGrain();
                this.saveQueue.AddToQueue(grain);
                this.saveQueue.MaximumCount = 0;
                this.saveQueue.EndWait();
                waitHandle.WaitOne();

                AssertGrainEventArgs(eventArgs, grain, "Test");
            }
        }

        /// <summary>
        /// Tests if failed grains are moved to the next queue if the next queue is not full.
        /// </summary>
        [Test]
        public void MoveFailedItemToNextQueueIfNextQueueNotFull()
        {
            using (var waitHandle1 = new EventWaitHandle(false, EventResetMode.AutoReset))
            using (var waitHandle2 = new EventWaitHandle(false, EventResetMode.AutoReset))
            {
                var grainRepository1 = new Mock<IGrainRepository>();
                grainRepository1.Setup(x => x.Save(It.IsAny<Grain>())).Callback(
                    new Action<Grain>(
                        g => 
                        {
                            throw new InvalidOperationException("Test");
                        }));

                Grain saved = null;
                var grainRepository2 = new Mock<IGrainRepository>();
                grainRepository2.Setup(x => x.Save(It.IsAny<Grain>())).Callback(
                    new Action<Grain>(
                        g =>
                        {
                            saved = g;
                            waitHandle2.Set();
                        }));

                var nextQueue = new SaveQueueMock(grainRepository2.Object) 
                {
                    MaximumCount = 1 
                };

                var queue = new SaveQueueMock(grainRepository1.Object) 
                { 
                    NextQueue = nextQueue
                };
               
                GrainEventArgs eventArgs = null;
                queue.GrainFailed += (sender, e) =>
                {
                    eventArgs = e;
                    waitHandle1.Set();
                };

                Grain grain = CreateSampleGrain();
                queue.AddToQueue(grain);
                
                queue.EndWait();
                waitHandle1.WaitOne();
                AssertGrainEventArgs(eventArgs, grain, "Test");
                
                nextQueue.EndWait();
                waitHandle2.WaitOne();
                
                Assert.That(saved, Is.SameAs(grain));

                nextQueue.EndWait();
                queue.EndWait();
            }
        }

        /// <summary>
        /// Tests if failed grains are re-enqueued if the next queue is full and the current queue is not full.
        /// </summary>
        [Test]
        public void EnqueueFailedItemIfNextQueueFullAndCurrentQueueNotFull()
        {
            var queue = new SaveQueueMock(this.grainRepositoryMock.Object)
            {
                MaximumCount = 1,
                NextQueue = new SaveQueueMock(this.grainRepositoryMock.Object)
                {
                    MaximumCount = 0
                }
            };
            
            this.AssertFailedItemEnqueued(queue);
        }

        /// <summary>
        /// Tests if failed grains are re-enqueued if there is no next queue and the current queue is not full.
        /// </summary>
        [Test]
        public void EnqueueFailedItemIfNoNextQueueAndCurrentQueueNotFull()
        {
            var queue = new SaveQueueMock(this.grainRepositoryMock.Object)
            {
                MaximumCount = 1,
                NextQueue = null
            };
            
            this.AssertFailedItemEnqueued(queue);
        }

        /// <summary>
        /// Tests if failed grains are discarded if the next queue is full and the current queue is full.
        /// </summary>
        [Test]
        public void DiscardFailedItemIfNextQueueFullAndCurrentQueueFull()
        {
            var queue = new SaveQueueMock(this.grainRepositoryMock.Object) 
            {
                MaximumCount = 1,
                NextQueue = new SaveQueueMock(this.grainRepositoryMock.Object) 
                {
                    MaximumCount = 0
                }
            };

            this.grainRepositoryMock.Setup(x => x.Save(It.IsAny<Grain>())).Callback(
                new Action<Grain>(
                    g =>
                    {
                        this.saveWaitHandle.Set();
                        throw new InvalidOperationException("Test");
                    }));

            using (var waitHandle = new EventWaitHandle(false, EventResetMode.AutoReset))
            {
                GrainEventArgs eventArgs = null;
                queue.GrainDiscarded += (sender, e) =>
                {
                    eventArgs = e;
                    waitHandle.Set();
                };

                Grain grain = CreateSampleGrain();
                queue.AddToQueue(grain);
                queue.MaximumCount = 0;
                queue.EndWait();
                waitHandle.WaitOne();

                AssertGrainEventArgs(eventArgs, grain, "Test");

                queue.EndWait();
            }
        }

        /// <summary>
        /// Tests if failed grains are discarded if there is no next queue and the current queue is full.
        /// </summary>
        [Test]
        public void DiscardFailedItemIfNoNextQueueAndCurrentQueueFull()
        {
            var queue = new SaveQueueMock(this.grainRepositoryMock.Object)
            {
                MaximumCount = 1, 
                NextQueue = null
            };

            this.grainRepositoryMock.Setup(x => x.Save(It.IsAny<Grain>())).Callback(
                new Action<Grain>(
                    g =>
                    {
                        this.saveWaitHandle.Set();
                        throw new InvalidOperationException("Test");
                    }));

            using (var waitHandle = new EventWaitHandle(false, EventResetMode.AutoReset))
            {
                GrainEventArgs eventArgs = null;
                queue.GrainDiscarded += (sender, e) =>
                {
                    eventArgs = e;
                    waitHandle.Set();
                };

                Grain grain = CreateSampleGrain();
                queue.AddToQueue(grain);
                queue.MaximumCount = 0;
                queue.EndWait();
                waitHandle.WaitOne();

                AssertGrainEventArgs(eventArgs, grain, "Test");

                queue.EndWait();
            }
        }

        /// <summary>
        /// Creates a sample Sahara grain.
        /// </summary>
        /// <returns>The created grain.</returns>
        private static Grain CreateSampleGrain()
        {
            return new Grain 
            {
                ApplicationName = "My application",
                EventId = "My event ID",
                GrainMessage = "My message",
                GrainType = "Grain type",
                Source = "My source"
            };
        }

        /// <summary>
        /// Asserts the contents of the specified <see cref="GrainEventArgs"/> object.
        /// </summary>
        /// <param name="eventArgs">The event arguments object.</param>
        /// <param name="grain">The grain object expected in the event arguments.</param>
        /// <param name="exceptionMessage">
        /// The expected exception message or <c>null</c> if exception is not expected.
        /// </param>
        private static void AssertGrainEventArgs(GrainEventArgs eventArgs, Grain grain, string exceptionMessage)
        {
            Assert.That(eventArgs, Is.Not.Null);
            Assert.That(eventArgs.Grain, Is.SameAs(grain));

            if (exceptionMessage != null) 
            {
                Assert.That(eventArgs.Exception, Is.Not.Null);
                Assert.That(eventArgs.Exception.Message, Is.EqualTo("Test"));
            }
            else 
            {
                Assert.That(eventArgs.Exception, Is.Null);
            }
        }

        /// <summary>
        /// Asserts that a failed item is enqueued on the specified queue.
        /// </summary>
        /// <param name="queue">The current queue.</param>
        private void AssertFailedItemEnqueued(SaveQueueMock queue)
        {
            this.grainRepositoryMock.Setup(x => x.Save(It.IsAny<Grain>())).Callback(
                new Action<Grain>(
                    g =>
                    {
                        this.saveWaitHandle.Set();
                        throw new InvalidOperationException("Test");
                    }));

            using (var waitHandle = new EventWaitHandle(false, EventResetMode.AutoReset))
            {
                GrainEventArgs eventArgs = null;
                queue.GrainFailed += (sender, e) =>
                {
                    eventArgs = e;
                    waitHandle.Set();
                };

                Grain grain = CreateSampleGrain();
                queue.AddToQueue(grain);
                queue.EndWait();
                this.WaitForSave();
                waitHandle.WaitOne();

                AssertGrainEventArgs(eventArgs, grain, "Test");

                queue.EndWait();
                this.WaitForSave();
                waitHandle.WaitOne();

                AssertGrainEventArgs(eventArgs, grain, "Test");

                queue.EndWait();
            }
        }

        /// <summary>
        /// Waits for a grain to be saved in the tested queue.
        /// </summary>
        private void WaitForSave()
        {
            this.saveWaitHandle.WaitOne();
        }
    }
}