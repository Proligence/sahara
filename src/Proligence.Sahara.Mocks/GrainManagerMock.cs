﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GrainManagerMock.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Sahara.Mocks
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using Proligence.Astoria.Client;
    using Proligence.Sahara.Client;

    /// <summary>
    /// Mocks the <see cref="IGrainManager"/> interface.
    /// </summary>
    public class GrainManagerMock : IGrainManager
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GrainManagerMock"/> class.
        /// </summary>
        public GrainManagerMock()
        {
            this.Grains = new List<Grain>();
        }

        /// <summary>
        /// Gets the grains saved by the mock service.
        /// </summary>
        [SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists", Justification = "By design.")]
        public List<Grain> Grains { get; private set; }

        /// <summary>
        /// Gets or sets the object returned by the <see cref="GetServiceInfo"/> method.
        /// </summary>
        public ServiceInfo ServiceInfoObject { get; set; }
        
        /// <summary>
        /// Gets the grain which was recently saved by the mock service.
        /// </summary>
        public Grain RecentlySavedGrain { get; private set; }

        /// <summary>
        /// Gets or sets the exception which will be thrown by the next method called on the service.
        /// </summary>
        public Exception Failure { get; set; }

        /// <summary>
        /// Gets information about the service.
        /// </summary>
        /// <returns>The <see cref="ServiceInfo"/> object which contains information about the service.</returns>
        public ServiceInfo GetServiceInfo()
        {
            this.SimulateFailure();
            return this.ServiceInfoObject;
        }

        /// <summary>
        /// Gets all watch dogs implemented by the service.
        /// </summary>
        /// <returns>A sequence of watch dog descriptors.</returns>
        public IEnumerable<WatchDogInfo> GetWatchDogs()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Invokes the specified watch dog.
        /// </summary>
        /// <param name="watchDogInfo">The watch dog to invoke.</param>
        /// <returns>The result of the invoked watch dog.</returns>
        public WatchDogResult InvokeWatchDog(WatchDogInfo watchDogInfo)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Invokes the specified extension.
        /// </summary>
        /// <param name="name">
        /// Extension name.
        /// </param>
        /// <param name="parameters">
        /// Extension parameters.
        /// </param>
        /// <returns>
        /// Result of the invocation.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// Thrown by default.
        /// </exception>
        public object InvokeExtension(string name, params object[] parameters) 
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Stores the specified <see cref="Grain"/> in Sahara.
        /// </summary>
        /// <param name="grain">The <see cref="Grain"/> object which represents the grain to save to Sahara.</param>
        public void SaveGrain(Grain grain)
        {
            this.SimulateFailure();
            this.Grains.Add(grain);
            this.RecentlySavedGrain = grain;
        }

        /// <summary>
        /// Gets the specified grains.
        /// </summary>
        /// <param name="applicationName">The name of the application for which grains will be returned.</param>
        /// <param name="count">The maximum number of grains to get.</param>
        /// <returns>
        /// A sequence of <see cref="Grain"/> objects which represent the result grains.
        /// </returns>
        public IEnumerable<Grain> GetGrains(string applicationName, int count)
        {
            this.SimulateFailure();
            return this.Grains
                .Where(g => g.ApplicationName == applicationName)
                .Take(count);
        }

        /// <summary>
        /// Gets the specified grains.
        /// </summary>
        /// <param name="applicationName">The name of the application for which grains will be returned.</param>
        /// <param name="eventId">The event identifier of the grains to get.</param>
        /// <param name="count">The maximum number of grains to get.</param>
        /// <returns>
        /// A sequence of <see cref="Grain"/> objects which represent the result grains.
        /// </returns>
        public IEnumerable<Grain> GetGrains(string applicationName, string eventId, int count)
        {
            this.SimulateFailure();
            return this.Grains
                .Where(g => g.ApplicationName == applicationName)
                .Where(g => g.EventId == eventId)
                .Take(count);
        }

        /// <summary>
        /// Gets the specified grains.
        /// </summary>
        /// <param name="applicationName">The name of the application for which grains will be returned.</param>
        /// <param name="maximumDate">The maximum date of to grains to get.</param>
        /// <returns>
        /// A sequence of <see cref="Grain"/> objects which represent the result grains.
        /// </returns>
        public IEnumerable<Grain> GetGrains(string applicationName, DateTime maximumDate)
        {
            this.SimulateFailure();
            return this.Grains
                .Where(g => g.ApplicationName == applicationName)
                .Where(g => g.Timestamp < maximumDate);
        }

        /// <summary>
        /// Gets the specified grains.
        /// </summary>
        /// <param name="applicationName">The name of the application for which grains will be returned.</param>
        /// <param name="eventId">The event identifier of the grains to get.</param>
        /// <param name="maximumDate">The maximum date of to grains to get.</param>
        /// <returns>
        /// A sequence of <see cref="Grain"/> objects which represent the result grains.
        /// </returns>
        public IEnumerable<Grain> GetGrains(string applicationName, string eventId, DateTime maximumDate)
        {
            this.SimulateFailure();
            return this.Grains
                .Where(g => g.ApplicationName == applicationName)
                .Where(g => g.EventId == eventId)
                .Where(g => g.Timestamp < maximumDate);
        }

        /// <summary>
        /// Removes all grains of the specified application.
        /// </summary>
        /// <param name="applicationName">The name of the application which grains will be deleted.</param>
        public void DeleteGrains(string applicationName)
        {
            this.SimulateFailure();
            this.Grains.RemoveAll(g => g.ApplicationName == applicationName);
        }

        /// <summary>
        /// Removes all grains of the specified application.
        /// </summary>
        /// <param name="applicationName">The name of the application which grains will be deleted.</param>
        /// <param name="maximumDate">The maximum date of to grains to delete.</param>
        public void DeleteGrains(string applicationName, DateTime maximumDate)
        {
            this.SimulateFailure();
            this.Grains.RemoveAll(g => g.ApplicationName == applicationName && g.Timestamp < maximumDate);
        }

        /// <summary>
        /// Throws the exception in the property <see cref="Failure"/> if the property is not set to <c>null</c>.
        /// </summary>
        private void SimulateFailure()
        {
            if (this.Failure != null)
            {
                throw this.Failure;
            }
        }
    }
}