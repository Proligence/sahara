﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AstoriaChannelFactoryMock.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Sahara.Mocks
{
    using System;
    using System.ServiceModel.Channels;
    using Proligence.Astoria.Client;
    using Proligence.Sahara.Client;

    /// <summary>
    /// Creates a <see cref="IAstoriaChannelFactory"/> implementation which returns mocked Sahara services.
    /// </summary>
    public class AstoriaChannelFactoryMock : IAstoriaChannelFactory
    {
        /// <summary>
        /// Gets or sets the <see cref="IGrainManager"/> service which will be returned by the <c>CreateChannel</c>
        /// method.
        /// </summary>
        public IGrainManager GrainManager { get; set; }

        /// <summary>
        /// Creates a channel to an Astoria service.
        /// </summary>
        /// <typeparam name="TApi">The interface that defines the API of the service.</typeparam>
        /// <param name="address">The url to the service.</param>
        /// <returns>The created service channel.</returns>
        public TApi CreateChannel<TApi>(string address)
            where TApi : class, IAstoriaService
        {
            if (typeof(TApi) == typeof(IGrainManager))
            {
                return (TApi)this.GrainManager;
            }

            throw new ArgumentException("The specified service API is not supported by the mock.");
        }

        /// <summary>
        /// Creates a channel to an Astoria service.
        /// </summary>
        /// <typeparam name="TApi">The interface that defines the API of the service.</typeparam>
        /// <param name="address">The url to the service.</param>
        /// <param name="configurationAction">
        /// An action which preforms configuration of the WCF channel factory.
        /// </param>
        /// <returns>The created service channel.</returns>
        public TApi CreateChannel<TApi>(string address, Action<IChannelFactory<TApi>> configurationAction)
            where TApi : class, IAstoriaService
        {
            return this.CreateChannel<TApi>(address);
        }
    }
}