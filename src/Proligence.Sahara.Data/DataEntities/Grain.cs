﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Grain.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Sahara.Data.DataEntities
{
    using System;

    /// <summary>
    /// Represents a Sahara grain object in the Sahara database.
    /// </summary>
    internal class Grain
    {
        /// <summary>
        /// Gets or sets the identifier of the grain.
        /// </summary>
        public virtual long GrainId { get; set; }

        /// <summary>
        /// Gets or sets the grain's application name.
        /// </summary>
        public virtual int ApplicationName { get; set; }

        /// <summary>
        /// Gets or sets the grains event id.
        /// </summary>
        public virtual int EventId { get; set; }

        /// <summary>
        /// Gets or sets the grain's source.
        /// </summary>
        public virtual int Source { get; set; }

        /// <summary>
        /// Gets or sets the grain's type.
        /// </summary>
        public virtual int GrainType { get; set; }

        /// <summary>
        /// Gets or sets the grain's message.
        /// </summary>
        public virtual int GrainMessage { get; set; }

        /// <summary>
        /// Gets or sets the type of the serialized data object.
        /// </summary>
        public virtual int? GrainDataType { get; set; }

        /// <summary>
        /// Gets or sets the grain's data.
        /// </summary>
        public virtual string GrainData { get; set; }

        /// <summary>
        /// Gets or sets the grain's creation time stamp.
        /// </summary>
        public virtual DateTime Timestamp { get; set; }

        /// <summary>
        /// Gets or sets the name of the user which created the grain.
        /// </summary>
        public virtual int UserName { get; set; }
    }
}