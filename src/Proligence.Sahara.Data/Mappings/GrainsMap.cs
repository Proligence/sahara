﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GrainsMap.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Sahara.Data.Mappings
{
    using System.Diagnostics.CodeAnalysis;
    using FluentNHibernate.Mapping;
    using Proligence.Sahara.Data.DataEntities;

    /// <summary>
    /// Defines NHibernate mappings for the <c>dbo.Grains</c> table.
    /// </summary>
    [SuppressMessage("Microsoft.Performance", "CA1812:AvoidUninstantiatedInternalClasses", 
        Justification = "The class is used by NHibernate through reflection")]
    internal sealed class GrainsMap : ClassMap<Grain>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GrainsMap"/> class.
        /// </summary>
        public GrainsMap()
        {
            this.Table("dbo.Grains");
            
            this.Id(x => x.GrainId);
            this.Map(x => x.ApplicationName);
            this.Map(x => x.EventId);
            this.Map(x => x.Source);
            this.Map(x => x.GrainType);
            this.Map(x => x.GrainMessage);
            this.Map(x => x.GrainDataType);
            this.Map(x => x.GrainData);
            this.Map(x => x.Timestamp);
            this.Map(x => x.UserName);
        }
    }
}