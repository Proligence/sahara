﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SaharaSessionFactoryCache.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Sahara.Data
{
    using System;
    using FluentNHibernate.Cfg;
    using FluentNHibernate.Cfg.Db;
    using NHibernate;
    using Proligence.Helpers.NHibernate;
    using Proligence.Sahara.Configuration;
    using Proligence.Sahara.Data.Mappings;

    /// <summary>
    /// Builds and caches the <see cref="ISessionFactory"/> instance used for accessing the Sahara database using 
    /// NHibernate.
    /// </summary>
    internal class SaharaSessionFactoryCache : SessionFactoryCache
    {
        /// <summary>
        /// The configuration of the Sahara service.
        /// </summary>
        private readonly SaharaConfiguration configuration;

        /// <summary>
        /// Initializes a new instance of the <see cref="SaharaSessionFactoryCache"/> class.
        /// </summary>
        /// <param name="configuration">The configuration of the Sahara service.</param>
        public SaharaSessionFactoryCache(SaharaConfiguration configuration)
        {
            if (configuration == null)
            {
                throw new ArgumentNullException("configuration");
            }

            this.configuration = configuration;
        }

        /// <summary>
        /// Gets the configuration of the NHibernate session factory.
        /// </summary>
        protected override FluentConfiguration Configuration
        {
            get
            {
                return Fluently.Configure()
                    .Database(MsSqlConfiguration.MsSql2008
                    .ConnectionString(builder => builder
                        .Server(this.configuration.Database.SqlServer)
                        .Database(this.configuration.Database.SqlDatabase)
                        .TrustedConnection()))
                    .Mappings(m => m.FluentMappings
                        .Add(typeof(GrainsMap))
                        .Add(typeof(StringTableMap)));
            }
        }
    }
}
