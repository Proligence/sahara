﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StringRepository.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Sahara.Data
{
    using System;
    using System.Data.SqlClient;
    using System.Linq;
    using NHibernate;
    using NHibernate.Criterion;
    using Proligence.Helpers.NHibernate;
    using Proligence.Sahara.Configuration;
    using Proligence.Sahara.Data.DataEntities;

    /// <summary>
    /// Implements saving and reading entries from Sahara's string table.
    /// </summary>
    public class StringRepository : RepositoryBase, IStringRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StringRepository"/> class.
        /// </summary>
        /// <param name="factoryCache">The object which caches the NHibernate session factory.</param>
        /// <param name="config">The configuration of the Sahara service.</param>
        public StringRepository(SessionFactoryCache factoryCache, SaharaConfiguration config)
            : base(factoryCache, new SqlConnection(config.Database.ConnectionString))
        {
        }

        /// <summary>
        /// Gets the identifier of the specified string. If the string is not present in the string table, then a new
        /// entry is created.
        /// </summary>
        /// <param name="str">The string value for which identifier will be returned.</param>
        /// <returns>The identifier of the specified string.</returns>
        public int GetStringId(string str)
        {
            if (str == null)
            {
                throw new ArgumentNullException("str");
            }

            return this.UsingSession(
                session =>
                    {
                        ICriteria query = session.CreateCriteria(typeof(StringTableEntry))
                            .Add(Restrictions.Eq("Value", str));

                        StringTableEntry entry = query.List<StringTableEntry>().FirstOrDefault();
                        if (entry != null) 
                        {
                            return entry.StringId;
                        }

                        StringTableEntry newEntry = new StringTableEntry { Value = str };
                        session.Save(newEntry);

                        return newEntry.StringId;
                    });
        }

        /// <summary>
        /// Gets the string with the specified identifier.
        /// </summary>
        /// <param name="id">The identifier of the string to get.</param>
        /// <returns>The value of the string or <c>null</c> if the string is not present in the string table.</returns>
        public string GetString(int id)
        {
            return this.UsingSession(
                session =>
                    {
                        var entry = session.Get(typeof(StringTableEntry), id) as StringTableEntry;
                        if (entry != null)
                        {
                            return entry.Value;
                        }

                        return null;
                    });
        }
    }
}