﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GrainRepository.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Sahara.Data
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using NHibernate;
    using NHibernate.Criterion;
    using Proligence.Helpers.NHibernate;
    using Proligence.Sahara.Client;
    using Proligence.Sahara.Configuration;
    using Grain = Proligence.Sahara.Data.DataEntities.Grain;

    /// <summary>
    /// Implements saving and reading grains from the Sahara database.
    /// </summary>
    public class GrainRepository : RepositoryBase, IGrainRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GrainRepository"/> class.
        /// </summary>
        /// <param name="factoryCache">The object which caches the NHibernate session factory.</param>
        /// <param name="config">The configuration of the Sahara service.</param>
        /// <param name="stringRepository">The string table repository.</param>
        public GrainRepository(
            SessionFactoryCache factoryCache, SaharaConfiguration config, IStringRepository stringRepository)
            : base(factoryCache, new SqlConnection(config.Database.ConnectionString))
        {
            this.StringRepository = stringRepository;
        }

        /// <summary>
        /// Gets or sets the string table repository.
        /// </summary>
        private IStringRepository StringRepository { get; set; }

        [SuppressMessage(
            "StyleCop.CSharp.DocumentationRules",
            "SA1600:ElementsMustBeDocumented",
            Justification = "Reviewed. Suppression is OK here.")]
        public void Save(Client.Grain obj, ISession session, ITransaction transaction) 
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Saves the specified grain in the Sahara database.
        /// </summary>
        /// <param name="obj">The grain to save.</param>
        public void Save(Client.Grain obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException("obj");
            }

            if (obj.ApplicationName == null)
            {
                throw new InvalidOperationException(
                    "The grain's property ApplicationName must be set to a non-null value.");
            }

            if (obj.EventId == null) 
            {
                throw new InvalidOperationException(
                    "The grain's property EventId must be set to a non-null value.");
            }

            if (obj.GrainMessage == null) 
            {
                throw new InvalidOperationException(
                    "The grain's property GrainMessage must be set to a non-null value.");
            }

            if (obj.GrainType == null) 
            {
                throw new InvalidOperationException(
                    "The grain's property GrainType must be set to a non-null value.");
            }

            if (obj.Source == null) 
            {
                throw new InvalidOperationException(
                    "The grain's property Source must be set to a non-null value.");
            }

            if (obj.UserName == null) 
            {
                throw new InvalidOperationException(
                    "The grain's property UserName must be set to a non-null value.");
            }

            var grain = new Grain 
            {
                ApplicationName = this.StringRepository.GetStringId(obj.ApplicationName),
                Source = this.StringRepository.GetStringId(obj.Source),
                GrainType = this.StringRepository.GetStringId(obj.GrainType),
                EventId = this.StringRepository.GetStringId(obj.EventId),
                GrainMessage = this.StringRepository.GetStringId(obj.GrainMessage),
                GrainDataType = obj.GrainDataType != null 
                                    ? this.StringRepository.GetStringId(obj.GrainDataType) 
                                    : (int?)null,
                GrainData = obj.GrainDataJson,
                Timestamp = obj.Timestamp,
                UserName = this.StringRepository.GetStringId(obj.UserName)
            };

            this.UsingSession(session => session.SaveOrUpdate(grain));

            obj.GrainId = grain.GrainId;
        }

        [SuppressMessage(
            "StyleCop.CSharp.DocumentationRules",
            "SA1600:ElementsMustBeDocumented",
            Justification = "Reviewed. Suppression is OK here.")]
        public void Delete(Client.Grain obj, ISession session, ITransaction transaction) 
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Deletes the specified grain from the Sahara database.
        /// </summary>
        /// <param name="obj">The grain to delete.</param>
        public void Delete(Client.Grain obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException("obj");
            }

            this.UsingSession(
                session =>
                    {
                        Grain grain = session.Get(typeof(Grain), obj.GrainId) as Grain;
                        session.Delete(grain);
                    });
        }

        [SuppressMessage(
            "StyleCop.CSharp.DocumentationRules",
            "SA1600:ElementsMustBeDocumented",
            Justification = "Reviewed. Suppression is OK here.")]
        public Client.Grain GetById(long objId, ISession session, ITransaction transaction) 
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Deletes the specified grain from the Sahara database.
        /// </summary>
        /// <param name="applicationName">The application name which grains to delete.</param>
        /// <param name="maximumDate">The maximum date of grains to delete.</param>
        public void Delete(string applicationName, DateTime? maximumDate = null)
        {
            if (string.IsNullOrWhiteSpace(applicationName)) 
            {
                throw new ArgumentNullException("applicationName");
            }

            this.UsingSession(
                session =>
                    {
                        int appStrId = this.StringRepository.GetStringId(applicationName);
                        ICriteria query =
                            session.CreateCriteria(typeof(Grain)).Add(Restrictions.Eq("ApplicationName", appStrId));

                        if (maximumDate != null)
                        {
                            query.Add(Restrictions.Le("Timestamp", maximumDate.Value));
                        }

                        Grain[] grains = query.List<Grain>().ToArray();
                        foreach (var grain in grains)
                        {
                            session.Delete(grain);
                        }
                    });
        }

        /// <summary>
        /// Gets the specified grain from the Sahara database.
        /// </summary>
        /// <param name="objId">The identifier of the grain to get from the database.</param>
        /// <returns>
        /// The grain retrieved from the repository or <c>null</c> if no grain was found with the specified identifier.
        /// </returns>
        public Client.Grain GetById(long objId)
        {
            Grain grain = this.UsingSession(session => session.Get(typeof(Grain), objId) as Grain);
            return this.ToClientGrain(grain);
        }

        [SuppressMessage(
            "StyleCop.CSharp.DocumentationRules",
            "SA1600:ElementsMustBeDocumented",
            Justification = "Reviewed. Suppression is OK here.")]
        public IEnumerable<Client.Grain> GetMany(IEnumerable<long> ids, ISession session, ITransaction transaction) 
        {
            throw new NotImplementedException();
        }

        [SuppressMessage(
            "StyleCop.CSharp.DocumentationRules",
            "SA1600:ElementsMustBeDocumented",
            Justification = "Reviewed. Suppression is OK here.")]
        public IEnumerable<Client.Grain> GetMany(IEnumerable<long> ids) 
        {
            throw new NotImplementedException();
        }

        [SuppressMessage(
            "StyleCop.CSharp.DocumentationRules",
            "SA1600:ElementsMustBeDocumented",
            Justification = "Reviewed. Suppression is OK here.")]
        public IEnumerable<Client.Grain> GetAll(ISession session, ITransaction transaction) 
        {
            throw new NotImplementedException();
        }

        [SuppressMessage(
            "StyleCop.CSharp.DocumentationRules", 
            "SA1600:ElementsMustBeDocumented", 
            Justification = "Reviewed. Suppression is OK here.")]
        public IEnumerable<Client.Grain> GetAll() 
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets grains of the specified application from the Sahara database.
        /// </summary>
        /// <param name="applicationName">The name of the application.</param>
        /// <param name="eventId">The event identifier of the grains to get.</param>
        /// <param name="maximumDate">The maximum date of the grains to get.</param>
        /// <param name="count">The maximum number of grains to get.</param>
        /// <returns>The grains returned from the database.</returns>
        public IEnumerable<Client.Grain> GetByApplication(
            string applicationName, string eventId = null, DateTime? maximumDate = null, int count = 1000)
        {
            if (string.IsNullOrWhiteSpace(applicationName))
            {
                throw new ArgumentNullException("applicationName");
            }

            return this.UsingSession(
                session =>
                    {
                        int appStrId = this.StringRepository.GetStringId(applicationName);
                        ICriteria query = session.CreateCriteria(typeof(Grain))
                            .Add(Restrictions.Eq("ApplicationName", appStrId))
                            .SetMaxResults(count)
                            .AddOrder(Order.Desc("GrainId"));

                        if (!string.IsNullOrWhiteSpace(eventId))
                        {
                            int eventStrId = this.StringRepository.GetStringId(eventId);
                            query.Add(Restrictions.Eq("EventId", eventStrId));
                        }

                        if (maximumDate != null)
                        {
                            query.Add(Restrictions.Le("Timestamp", maximumDate.Value));
                        }

                        return query.List<Grain>().Select(this.ToClientGrain).ToArray();
                    });
        }

        /// <summary>
        /// Converts a data entity grain object into a <see cref="Client.Grain"/> object.
        /// </summary>
        /// <param name="grain">The data entity grain object.</param>
        /// <returns>The created <see cref="Client.Grain"/> object.</returns>
        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes",
            Justification = "We want to catch all exceptions in this case.")]
        private Client.Grain ToClientGrain(Grain grain)
        {
            var result = new Client.Grain
                {
                    GrainId = grain.GrainId,
                    ApplicationName = this.StringRepository.GetString(grain.ApplicationName),
                    Source = this.StringRepository.GetString(grain.Source),
                    GrainType = this.StringRepository.GetString(grain.GrainType),
                    EventId = this.StringRepository.GetString(grain.EventId),
                    GrainMessage = this.StringRepository.GetString(grain.GrainMessage),
                    Timestamp = grain.Timestamp,
                    UserName = this.StringRepository.GetString(grain.UserName)
                };

            if ((grain.GrainDataType != null) && (grain.GrainData != null))
            {
                try
                {
                    result.GrainDataType = this.StringRepository.GetString(grain.GrainDataType.Value);
                    result.GrainDataJson = grain.GrainData;
                }
                catch (Exception ex)
                {
                    result.GrainData = new ExceptionGrainData(ex);
                }
            }

            return result;
        }
    }
}