// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IGrainRepository.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Sahara.Data
{
    using System;
    using System.Collections.Generic;
    using Proligence.Helpers.NHibernate;

    /// <summary>
    /// Defines the API of the grain data repository.
    /// </summary>
    public interface IGrainRepository : IRepository<Client.Grain, long>
    {
        /// <summary>
        /// Deletes the specified grain from the Sahara database.
        /// </summary>
        /// <param name="applicationName">The application name which grains to delete.</param>
        /// <param name="maximumDate">The maximum date of grains to delete.</param>
        void Delete(string applicationName, DateTime? maximumDate = null);

        /// <summary>
        /// Gets grains of the specified application from the Sahara database.
        /// </summary>
        /// <param name="applicationName">The name of the application.</param>
        /// <param name="eventId">The event identifier of the grains to get.</param>
        /// <param name="maximumDate">The maximum date of the grains to get.</param>
        /// <param name="count">The maximum number of grains to get.</param>
        /// <returns>The grains returned from the database.</returns>
        IEnumerable<Client.Grain> GetByApplication(
            string applicationName, string eventId = null, DateTime? maximumDate = null, int count = 1000);
    }
}