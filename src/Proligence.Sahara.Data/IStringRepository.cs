﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IStringRepository.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Sahara.Data
{
    /// <summary>
    /// Defines the API of the string table data repository.
    /// </summary>
    public interface IStringRepository
    {
        /// <summary>
        /// Gets the identifier of the specified string. If the string is not present in the string table, then a new
        /// entry is created.
        /// </summary>
        /// <param name="str">The string value for which identifier will be returned.</param>
        /// <returns>The identifier of the specified string.</returns>
        int GetStringId(string str);

        /// <summary>
        /// Gets the string with the specified identifier.
        /// </summary>
        /// <param name="id">The identifier of the string to get.</param>
        /// <returns>The value of the string or <c>null</c> if the string is not present in the string table.</returns>
        string GetString(int id);
    }
}