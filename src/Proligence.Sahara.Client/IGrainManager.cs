﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IGrainManager.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Sahara.Client
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ServiceModel;
    using Proligence.Astoria.Client;

    /// <summary>
    /// Defines the API of the Sahara Grain Manager service.
    /// </summary>
    [ServiceContract]
    [Description("Defines a Sahara-based logging API.")]
    public interface IGrainManager : IAstoriaService
    {
        /// <summary>
        /// Stores the specified <see cref="Grain"/> in Sahara.
        /// </summary>
        /// <param name="grain">The <see cref="Grain"/> object which represents the grain to save to Sahara.</param>
        [OperationContract(Name = "SaveGrain")]
        void SaveGrain(Grain grain);

        /// <summary>
        /// Gets the specified grains.
        /// </summary>
        /// <param name="applicationName">The name of the application for which grains will be returned.</param>
        /// <param name="count">The maximum number of grains to get.</param>
        /// <returns>A sequence of <see cref="Grain"/> objects which represent the result grains.</returns>
        [OperationContract(Name = "GetGrains")]
        IEnumerable<Grain> GetGrains(string applicationName, int count);

        /// <summary>
        /// Gets the specified grains.
        /// </summary>
        /// <param name="applicationName">The name of the application for which grains will be returned.</param>
        /// <param name="eventId">The event identifier of the grains to get.</param>
        /// <param name="count">The maximum number of grains to get.</param>
        /// <returns>A sequence of <see cref="Grain"/> objects which represent the result grains.</returns>
        [OperationContract(Name = "GetGrainsByEventId")]
        IEnumerable<Grain> GetGrains(string applicationName, string eventId, int count);

        /// <summary>
        /// Gets the specified grains.
        /// </summary>
        /// <param name="applicationName">The name of the application for which grains will be returned.</param>
        /// <param name="maximumDate">The maximum date of to grains to get.</param>
        /// <returns>A sequence of <see cref="Grain"/> objects which represent the result grains.</returns>
        [OperationContract(Name = "GetGrainsByDate")]
        IEnumerable<Grain> GetGrains(string applicationName, DateTime maximumDate);

        /// <summary>
        /// Gets the specified grains.
        /// </summary>
        /// <param name="applicationName">The name of the application for which grains will be returned.</param>
        /// <param name="eventId">The event identifier of the grains to get.</param>
        /// <param name="maximumDate">The maximum date of to grains to get.</param>
        /// <returns>A sequence of <see cref="Grain"/> objects which represent the result grains.</returns>
        [OperationContract(Name = "GetGrainsGrainsByEventIdAndDate")]
        IEnumerable<Grain> GetGrains(string applicationName, string eventId, DateTime maximumDate);
        
        /// <summary>
        /// Removes all grains of the specified application.
        /// </summary>
        /// <param name="applicationName">The name of the application which grains will be deleted.</param>
        [OperationContract(Name = "GetGrainsByApplication")]
        void DeleteGrains(string applicationName);

        /// <summary>
        /// Removes all grains of the specified application.
        /// </summary>
        /// <param name="applicationName">The name of the application which grains will be deleted.</param>
        /// <param name="maximumDate">The maximum date of to grains to delete.</param>
        [OperationContract(Name = "GetGrainsByApplicationAndDate")]
        void DeleteGrains(string applicationName, DateTime maximumDate);
    }
}