// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Grain.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Sahara.Client
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Reflection;
    using System.Runtime.Serialization;
    using System.Runtime.Serialization.Json;
    using System.Text;

    /// <summary>
    /// Represents a single Sahara grain.
    /// </summary>
    [DataContract]
    public class Grain 
    {
        /// <summary>
        /// Stores the grain's data object serialized into JSON format.
        /// </summary>
        [DataMember]
        private string grainDataJson;

        /// <summary>
        /// Stores the grain's data object instance.
        /// </summary>
        private object grainData;

        /// <summary>
        /// Initializes a new instance of the <see cref="Grain"/> class.
        /// </summary>
        public Grain()
        {
            MethodBase callingMethod = new StackFrame(1).GetMethod();
            this.Source = callingMethod.DeclaringType.FullName + "." + callingMethod.Name;

            this.Timestamp = DateTime.Now;
            this.UserName = Environment.UserName;
        }

        /// <summary>
        /// Gets or sets the unique identifier of the grain.
        /// </summary>
        [DataMember]
        public long GrainId { get; set; }

        /// <summary>
        /// Gets or sets the name of the application which created the grain.
        /// </summary>
        [DataMember]
        public string ApplicationName { get; set; }

        /// <summary>
        /// Gets or sets the source of the grain (usually name of the method which created the grain).
        /// </summary>
        [DataMember]
        public string Source { get; set; }

        /// <summary>
        /// Gets or sets the type of the grain (warning, error, info, etc.).
        /// </summary>
        [DataMember]
        public string GrainType { get; set; }

        /// <summary>
        /// Gets or sets the identifier of the application-specific event that is described by the grain.
        /// </summary>
        /// <remarks>
        /// This text will be saved in a database-global dictionary and therefore it should not contain entry-specific 
        /// data such as identifiers, file names, time stamps, etc. (such information should be stored in the object
        /// <see cref="GrainData"/>.
        /// </remarks>
        [DataMember]
        public string EventId { get; set; }

        /// <summary>
        /// Gets or sets the text of the grain's message.
        /// </summary>
        /// <remarks>
        /// This text will be saved in a database-global dictionary and therefore it should not contain entry-specific 
        /// data such as identifiers, file names, time stamps, etc. (such information should be stored in the object
        /// <see cref="GrainData"/>.
        /// </remarks>
        [DataMember]
        public string GrainMessage { get; set; }

        /// <summary>
        /// Gets or sets the object which will be serialized to the grain.
        /// </summary>
        /// <remarks>
        /// This object will be serialized to JSON and stored in the grain.
        /// </remarks>
        public object GrainData 
        { 
            get
            {
                return this.grainData;
            }

            set
            {
                if (value != null)
                {
                    this.GrainDataType = value.GetType().AssemblyQualifiedName;
                    this.grainDataJson = SerializeGrainData(value);   
                }
                else
                {
                    this.GrainDataType = null;
                    this.grainDataJson = null;
                }

                this.grainData = value;
            }
        }

        /// <summary>
        /// Gets or sets the grain's data object serialized into JSON format.
        /// </summary>
        public string GrainDataJson
        {
            get
            {
                return this.grainDataJson;
            }

            set
            {
                if ((value != null) && (this.GrainDataType != null))
                {
                    this.grainData = DeserializeGrainData(this.GrainDataType, value);
                }
                else
                {
                    this.grainData = null;
                }

                this.grainDataJson = value;
            }
        }

        /// <summary>
        /// Gets or sets the type name of the grain's data object.
        /// </summary>
        [DataMember]
        public string GrainDataType { get; set; }

        /// <summary>
        /// Gets or sets the date and time when the grain was created.
        /// </summary>
        [DataMember]
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// Gets or sets the name of the user who created the grain.
        /// </summary>
        [DataMember]
        public string UserName { get; set; }

        /// <summary>
        /// Serializes the specified grain data object to text.
        /// </summary>
        /// <param name="grainData">The object which will be serialized.</param>
        /// <returns>The result of the serialization.</returns>
        private static string SerializeGrainData(object grainData)
        {
            if (grainData == null)
            {
                return null;
            }

            var serializer = new DataContractJsonSerializer(grainData.GetType());
            using (var ms = new MemoryStream())
            {
                serializer.WriteObject(ms, grainData);
                string json = Encoding.UTF8.GetString(ms.ToArray());
                return json;
            }
        }

        /// <summary>
        /// Deserializes a grain data object.
        /// </summary>
        /// <param name="typeName">The name of the type of the object to deserialize.</param>
        /// <param name="grainData">The serialized grain data.</param>
        /// <returns>The result of the deserialization.</returns>
        private static object DeserializeGrainData(string typeName, string grainData)
        {
            if (string.IsNullOrWhiteSpace(typeName) || string.IsNullOrEmpty(grainData))
            {
                return null;
            }

            var type = Type.GetType(typeName);
            var serializer = new DataContractJsonSerializer(type);

            using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(grainData)))
            {
                return serializer.ReadObject(stream);
            }
        }
    }
}