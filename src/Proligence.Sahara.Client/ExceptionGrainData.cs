﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExceptionGrainData.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Sahara.Client
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Stores exception details in Sahara grains.
    /// </summary>
    [DataContract]
    public class ExceptionGrainData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ExceptionGrainData"/> class.
        /// </summary>
        public ExceptionGrainData()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExceptionGrainData"/> class.
        /// </summary>
        /// <param name="exception">The exception from which data will be stored.</param>
        public ExceptionGrainData(Exception exception)
        {
            if (exception == null)
            {
                throw new ArgumentNullException("exception");
            }

            this.ExceptionType = exception.GetType().Name;
            this.Message = exception.Message;
            this.StackTrace = exception.StackTrace;
            this.Source = exception.Source;

            if (exception.InnerException != null)
            {
                this.InnerException = new ExceptionGrainData(exception.InnerException);
            }
        }

        /// <summary>
        /// Gets or sets the type of the exception.
        /// </summary>
        [DataMember(Name = "t", EmitDefaultValue = false)]
        public string ExceptionType { get; set; }

        /// <summary>
        /// Gets or sets the exception's message.
        /// </summary>
        [DataMember(Name = "msg", EmitDefaultValue = false)]
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the exception's stack trace.
        /// </summary>
        [DataMember(Name = "st", EmitDefaultValue = false)]
        public string StackTrace { get; set; }

        /// <summary>
        /// Gets or sets the source of the exception.
        /// </summary>
        [DataMember(Name = "src", EmitDefaultValue = false)]
        public string Source { get; set; }

        /// <summary>
        /// Gets or sets the data of the inner exception.
        /// </summary>
        [DataMember(Name = "ie", EmitDefaultValue = false)]
        public ExceptionGrainData InnerException { get; set; }
    }
}