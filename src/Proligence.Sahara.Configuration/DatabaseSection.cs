﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DatabaseSection.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Sahara.Configuration
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Represents the Database configuration section.
    /// </summary>
    [DataContract]
    public class DatabaseSection
    {
        /// <summary>
        /// Test setting.
        /// </summary>
        [DataMember(Name = "SqlDatabase", EmitDefaultValue = false)]
        private string sqlDatabase;

        /// <summary>
        /// The name of the SQL server which stores the data of the service.
        /// </summary>
        [DataMember(Name = "SqlServer", EmitDefaultValue = false)]
        private string sqlServer;

        /// <summary>
        /// Gets the test setting.
        /// </summary>
        public string SqlDatabase
        {
            get { return this.sqlDatabase; }
        }

        /// <summary>
        /// Gets the name of the SQL server which stores the data of the service.
        /// </summary>
        public string SqlServer
        {
            get { return this.sqlServer; }
        }

        /// <summary>
        /// Gets the connection string to the SQL server which stores the data of the service.
        /// </summary>
        public string ConnectionString
        {
            get
            {
                return "Data Source=" + this.SqlServer + ";Database=" + this.SqlDatabase + ";Integrated Security=SSPI";
            }
        }
    }
}