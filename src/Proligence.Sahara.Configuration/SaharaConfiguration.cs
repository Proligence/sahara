﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SaharaConfiguration.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Sahara.Configuration
{
    using System.Runtime.Serialization;
    using Proligence.Astoria.Configuration;

    /// <summary>
    /// Represents the configuration of the Sahara service.
    /// </summary>
    [DataContract]
    public class SaharaConfiguration : AstoriaServiceConfiguration
    {
        /// <summary>
        /// The Database configuration section.
        /// </summary>
        [DataMember(Name = "Database")]
        private DatabaseSection database;

        /// <summary>
        /// The SaveQueues configuration section.
        /// </summary>
        [DataMember(Name = "SaveQueues")]
        private SaveQueuesSection saveQueues;

        /// <summary>
        /// Gets the Database configuration section.
        /// </summary>
        public DatabaseSection Database
        {
            get { return this.database; }
        }

        /// <summary>
        /// Gets the SaveQueues configuration section.
        /// </summary>
        public SaveQueuesSection SaveQueues
        {
            get { return this.saveQueues; }
        }
    }
}