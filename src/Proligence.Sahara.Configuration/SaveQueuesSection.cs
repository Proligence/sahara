﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SaveQueuesSection.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Sahara.Configuration
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Represents the Save Queues configuration section.
    /// </summary>
    [DataContract]
    public class SaveQueuesSection
    {
        /// <summary>
        /// The time interval (in seconds) between processing grains in the first queue.
        /// </summary>
        [DataMember(Name = "Level1Interval", EmitDefaultValue = false)]
        private int level1Interval;

        /// <summary>
        /// The time interval (in seconds) between processing grains in the second queue.
        /// </summary>
        [DataMember(Name = "Level2Interval", EmitDefaultValue = false)]
        private int level2Interval;

        /// <summary>
        /// The time interval (in seconds) between processing grains in the last queue.
        /// </summary>
        [DataMember(Name = "Level3Interval", EmitDefaultValue = false)]
        private int level3Interval;

        /// <summary>
        /// The maximum number of items in the last queue.
        /// </summary>
        [DataMember(Name = "MaxQueueItems", EmitDefaultValue = false)]
        private int maxQueueItems;

        /// <summary>
        /// Gets the time interval (in seconds) between processing grains in the first queue.
        /// </summary>
        public int Level1Interval
        {
            get { return this.level1Interval; }
        }

        /// <summary>
        /// Gets the time interval (in seconds) between processing grains in the second queue.
        /// </summary>
        public int Level2Interval
        {
            get { return this.level2Interval; }
        }

        /// <summary>
        /// Gets the time interval (in seconds) between processing grains in the last queue.
        /// </summary>
        public int Level3Interval
        {
            get { return this.level3Interval; }
        }

        /// <summary>
        /// Gets the maximum number of items in the last queue.
        /// </summary>
        public int MaxQueueItems
        {
            get { return this.maxQueueItems; }
        }
    }
}