﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Sahara.Service
{
    using Proligence.Astoria.ServiceModel;
    using Proligence.Sahara.Client;
    using Proligence.Sahara.Configuration;
    using Proligence.Sahara.Core;

    /// <summary>
    /// Implements the service's entry point.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the service.
        /// </summary>
        /// <param name="args">The application's command line arguments.</param>
        public static void Main(string[] args)
        {
            var entry = new ServiceEntryPoint<GrainManager, IGrainManager, SaharaConfiguration> 
            {
                TypesToResolve = new[]
                {
                    typeof(Proligence.Sahara.Core.GrainManager), 
                    typeof(Proligence.Sahara.Data.IGrainRepository)
                }
            };

            entry.Run(args);
        }
    }
}