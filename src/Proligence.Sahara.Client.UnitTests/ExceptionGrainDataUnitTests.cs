﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExceptionGrainDataUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Sahara.Client.UnitTests
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using NUnit.Framework;

    /// <summary>
    /// Implements unit tests for the <see cref="ExceptionGrainData"/> class.
    /// </summary>
    [TestFixture]
    public class ExceptionGrainDataUnitTests
    {
        /// <summary>
        /// Test exception instance.
        /// </summary>
        private ExceptionMock exception;

        /// <summary>
        /// Sets up the test fixture before each test.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.exception = new ExceptionMock("My exception message");
        }

        /// <summary>
        /// Tests if the constructor of the <see cref="ExceptionGrainData"/> class properly initializes the 
        /// <see cref="ExceptionGrainData.ExceptionType"/> property.
        /// </summary>
        [Test]
        public void TestExceptionTypeInitialization()
        {
            var data = new ExceptionGrainData(this.exception);

            Assert.That(data.ExceptionType, Is.EqualTo("ExceptionMock"));
        }

        /// <summary>
        /// Tests if the constructor of the <see cref="ExceptionGrainData"/> class properly initializes the 
        /// <see cref="ExceptionGrainData.Message"/> property.
        /// </summary>
        [Test]
        public void TestMessageInitialization()
        {
            var data = new ExceptionGrainData(this.exception);

            Assert.That(data.Message, Is.EqualTo(this.exception.Message));
        }

        /// <summary>
        /// Tests if the constructor of the <see cref="ExceptionGrainData"/> class properly initializes the 
        /// <see cref="ExceptionGrainData.StackTrace"/> property.
        /// </summary>
        [Test]
        public void TestStackTraceInitialization()
        {
            var data = new ExceptionGrainData(this.exception);

            Assert.That(data.StackTrace, Is.EqualTo(this.exception.StackTrace));
        }

        /// <summary>
        /// Tests if the constructor of the <see cref="ExceptionGrainData"/> class properly initializes the 
        /// <see cref="ExceptionGrainData.Source"/> property.
        /// </summary>
        [Test]
        public void TestSourceInitialization()
        {
            this.exception.Source = "My source";
            var data = new ExceptionGrainData(this.exception);

            Assert.That(data.Source, Is.EqualTo(this.exception.Source));
        }

        /// <summary>
        /// Tests if the constructor of the <see cref="ExceptionGrainData"/> class properly initializes the 
        /// <see cref="ExceptionGrainData.InnerException"/> property when the specified exception doesn't have an 
        /// inner exception.
        /// </summary>
        [Test]
        public void TestInnerExceptionInitializationWhenInnerExceptionNull()
        {
            var data = new ExceptionGrainData(this.exception);

            Assert.That(data.InnerException, Is.Null);
        }

        /// <summary>
        /// Tests if the constructor of the <see cref="ExceptionGrainData"/> class properly initializes the 
        /// <see cref="ExceptionGrainData.InnerException"/> property when the specified exception has an inner 
        /// exception.
        /// </summary>
        [Test]
        public void TestInnerExceptionInitializationWhenInnerExceptionNotNull()
        {
            var inner = new ExceptionMock("My inner exception message.");
            var outer = new ExceptionMock("My exception message.", inner);
            var data = new ExceptionGrainData(outer);

            Assert.That(data.InnerException, Is.Not.Null);
            Assert.That(data.InnerException.Message, Is.EqualTo(inner.Message));
        }

        /// <summary>
        /// Custom exception for testing purposes.
        /// </summary>
        [Serializable]
        [SuppressMessage("Microsoft.Design", "CA1032:ImplementStandardExceptionConstructors", Justification = "Test")]
        [SuppressMessage("Microsoft.Design", "CA1064:ExceptionsShouldBePublic", Justification = "Test")]
        private sealed class ExceptionMock : Exception
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="ExceptionMock"/> class.
            /// </summary>
            /// <param name="message">The exception's message.</param>
            public ExceptionMock(string message)
                : base(message)
            {
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="ExceptionMock"/> class.
            /// </summary>
            /// <param name="message">The exception's message.</param>
            /// <param name="innerException">The inner exception.</param>
            public ExceptionMock(string message, Exception innerException)
                : base(message, innerException)
            {
            }

            /// <summary>
            /// Gets or sets the name of the application or the object that causes the error.
            /// </summary>
            /// <returns>The name of the application or the object that causes the error.</returns>
            public new string Source
            {
                get { return base.Source; }
                set { base.Source = value; }
            }
        }
    }
}