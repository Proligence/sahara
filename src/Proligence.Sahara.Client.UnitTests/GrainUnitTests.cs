﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GrainUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Sahara.Client.UnitTests
{
    using System;
    using NUnit.Framework;
    using Proligence.Sahara.Client;

    /// <summary>
    /// Implements unit tests for the <see cref="Grain"/> class.
    /// </summary>
    [TestFixture]
    public class GrainUnitTests
    {
        /// <summary>
        /// Tests if the <see cref="Grain.Source"/> property is initialized by default to the full name of the method
        /// which created the grain.
        /// </summary>
        [Test]
        public void TestDefaultSourceInitialization()
        {
            var grain = new Grain();

            Assert.That(
                grain.Source, 
                Is.EqualTo("Proligence.Sahara.Client.UnitTests.GrainUnitTests.TestDefaultSourceInitialization"));
        }

        /// <summary>
        /// Tests if the <see cref="Grain.Timestamp"/> property is initialized by default to the current date and time.
        /// </summary>
        [Test]
        public void TestDefaultTimestampInitialization()
        {
            var grain = new Grain();

            Assert.That(grain.Timestamp, Is.EqualTo(DateTime.Now).Within(new TimeSpan(0, 0, 0, 0, 10)));
        }

        /// <summary>
        /// Tests if the <see cref="Grain.UserName"/> property is initialized by default to the current user name.
        /// </summary>
        [Test]
        public void TestDefaultUserNameInitialization()
        {
            var grain = new Grain();

            Assert.That(grain.UserName, Is.EqualTo(Environment.UserName));
        }

        /// <summary>
        /// Tests if setting the grain's data object works correctly.
        /// </summary>
        [Test]
        public void SetDataObject()
        {
            var dataObject = new ExceptionGrainData(new InvalidOperationException("Exception message"));

            var grain = new Grain();
            grain.GrainData = dataObject;

            Assert.That(grain.GrainData, Is.SameAs(dataObject));
            Assert.That(grain.GrainDataType, Is.EqualTo(typeof(ExceptionGrainData).AssemblyQualifiedName));
            Assert.That(
                grain.GrainDataJson, 
                Is.EqualTo(@"{""msg"":""Exception message"",""t"":""InvalidOperationException""}"));
        }

        /// <summary>
        /// Tests if <c>null</c> can be set as the grain's data object.
        /// </summary>
        [Test]
        public void SetNullDataObject()
        {
            var grain = new Grain();
            grain.GrainData = null;

            Assert.That(grain.GrainData, Is.Null);
            Assert.That(grain.GrainDataType, Is.Null);
            Assert.That(grain.GrainDataJson, Is.Null);
        }

        /// <summary>
        /// Tests if setting the grain's data object as JSON deserializes the data object.
        /// </summary>
        [Test]
        public void SetDataObjectJson()
        {
            var grain = new Grain();
            grain.GrainDataType = typeof(ExceptionGrainData).AssemblyQualifiedName;
            grain.GrainDataJson = @"{""msg"":""Exception message"",""t"":""InvalidOperationException""}";

            ExceptionGrainData dataObject = (ExceptionGrainData)grain.GrainData;
            Assert.That(dataObject.Message, Is.EqualTo("Exception message"));
        }

        /// <summary>
        /// Tests if setting <c>null</c> as the grain's data object JSON does not cause an error.
        /// </summary>
        [Test]
        public void SetNullDataObjectJson()
        {
            var grain = new Grain();
            grain.GrainDataType = null;
            grain.GrainDataJson = null;

            Assert.That(grain.GrainData, Is.Null);
        }

        /// <summary>
        /// Tests if setting the grain's data object JSON without data object type name set does not cause an error.
        /// </summary>
        [Test]
        public void SetDataObjectJsonWithoutDataObjectTypeName()
        {
            var grain = new Grain();
            grain.GrainDataJson = @"{""msg"":""Exception message"",""t"":""InvalidOperationException""}";

            Assert.That(grain.GrainData, Is.Null);
        }
    }
}