﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SaharaConfigurationUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Sahara.Configuration.UnitTests
{
    using NUnit.Framework;
    using Proligence.Astoria.Configuration;

    /// <summary>
    /// Implements unit tests for the configuration of the Sahara service.
    /// </summary>
    [TestFixture]
    public class SaharaConfigurationUnitTests
    {
        /// <summary>
        /// The configuration read from the test configuration file.
        /// </summary>
        private SaharaConfiguration configuration;

        /// <summary>
        /// Sets up the test fixture.
        /// </summary>
        [TestFixtureSetUp]
        public void Setup()
        {
            var configurationProvider = new DataContractSerializationConfigurationProvider<SaharaConfiguration>();
            this.configuration = configurationProvider.GetConfiguration("test.config");
        }

        /// <summary>
        /// Checks the value of the <see cref="ServiceSection.Address"/> setting.
        /// </summary>
        [Test]
        public void ServiceAddress()
        {
            Assert.That(
                this.configuration.Service.Address, 
                Is.EqualTo("net.tcp://localhost:81/Proligence/Sahara/GrainManager"));
        }

        /// <summary>
        /// Checks the value of the <see cref="DatabaseSection.SqlDatabase"/> setting.
        /// </summary>
        [Test]
        public void DatabaseSqlDatabase()
        {
            Assert.That(this.configuration.Database.SqlDatabase, Is.EqualTo("Sahara"));
        }

        /// <summary>
        /// Checks the value of the <see cref="DatabaseSection.SqlServer"/> setting.
        /// </summary>
        [Test]
        public void DatabaseSqlServer()
        {
            Assert.That(this.configuration.Database.SqlServer, Is.EqualTo("JUNKTOWN"));
        }

        /// <summary>
        /// Checks the value of the <see cref="SaveQueuesSection.Level1Interval"/> setting.
        /// </summary>
        [Test]
        public void SaveQueuesLevel1Interval()
        {
            Assert.That(this.configuration.SaveQueues.Level1Interval, Is.EqualTo(0));
        }

        /// <summary>
        /// Checks the value of the <see cref="SaveQueuesSection.Level2Interval"/> setting.
        /// </summary>
        [Test]
        public void SaveQueuesLevel2Interval()
        {
            Assert.That(this.configuration.SaveQueues.Level2Interval, Is.EqualTo(60));
        }

        /// <summary>
        /// Checks the value of the <see cref="SaveQueuesSection.Level3Interval"/> setting.
        /// </summary>
        [Test]
        public void SaveQueuesLevel3Interval()
        {
            Assert.That(this.configuration.SaveQueues.Level3Interval, Is.EqualTo(900));
        }

        /// <summary>
        /// Checks the value of the <see cref="SaveQueuesSection.MaxQueueItems"/> setting.
        /// </summary>
        [Test]
        public void SaveQueuesMaxQueueItems()
        {
            Assert.That(this.configuration.SaveQueues.MaxQueueItems, Is.EqualTo(10000));
        }
    }
}